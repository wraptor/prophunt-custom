if SERVER then
	AddCSLuaFile()
	AddCSLuaFile("pointshop/cl_init.lua")
	AddCSLuaFile("cl_init.lua")
	AddCSLuaFile("cl_menu.lua")
	AddCSLuaFile("sh_config.lua")
	AddCSLuaFile("sh_init.lua")
	AddCSLuaFile("sh_player.lua")
end
if CLIENT then
	include("pointshop/cl_init.lua")
	include("PERMSystem/cl_init.lua")
	include("taunting/cl_init.lua")
	include("sh_init.lua")
	include("cl_menu.lua")
end