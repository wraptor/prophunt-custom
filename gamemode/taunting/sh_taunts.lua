TAUNTS = {}
TAUNTS.HUNTERS = {
	-- Public (For everyone)
	--  Main, build in versions.
	[1] = {
		Name = "Come to papa!",
		Sound = "taunts/hunters/come_to_papa.wav",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[2] = {
		Name = "I'm your father",
		Sound = "taunts/hunters/father.mp3",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[3] = {
		Name = "Taking fire",
		Sound = "taunts/hunters/fireassis.wav",
		Reward = 0,
		Duration = 2,
		GroupsOnly = false
	},
	[4] = {
		Name = "Portal past of being alive club",
		Sound = "taunts/hunters/glados-president.wav",
		Reward = 0,
		Duration = 5,
		GroupsOnly = false
	},
	[5] = {
		Name = "Hit, need assistance",
		Sound = "taunts/hunters/hitassist.wav",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[6] = {
		Name = "I will look for you",
		Sound = "taunts/hunters/illfindyou.mp3",
		Reward = 0,
		Duration = 6,
		GroupsOnly = false
	},
	[7] = {
		Name = "Radio laughing",
		Sound = "taunts/hunters/laugh.wav",
		Reward = 0,
		Duration = 2,
		GroupsOnly = false
	},
	[8] = {
		Name = "Now what?",
		Sound = "taunts/hunters/now_what.wav",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[9] = {
		Name = "Oh Rude!",
		Sound = "taunts/hunters/rude.mp3",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[10] = {
		Name = "No soul!",
		Sound = "taunts/hunters/soul.mp3",
		Reward = 0,
		Duration = 5,
		GroupsOnly = false
	},
	[11] = {
		Name = "Power of the dark side",
		Sound = "taunts/hunters/you_dont_know_the_power.wav",
		Reward = 0,
		Duration = 4,
		GroupsOnly = false
	},
	[12] = {
		Name = "Underestimate the dark side",
		Sound = "taunts/hunters/you_underestimate_the_power.wav",
		Reward = 0,
		Duration = 4,
		GroupsOnly = false
	},
	[13] = {
		Name = "Forgive my interruption",
		Sound = "taunts/props_extra/dx_augmented.wav",
		Reward = 0,
		Duration = 2,
		GroupsOnly = false
	},
	[14] = {
		Name = "Guhuuhuaaa",
		Sound = "vo/k_lab/ba_guh.wav",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	},
	[15] = {
		Name = "The hacks!",
		Sound = "vo/npc/male01/thehacks01.wav",
		Reward = 0,
		Duration = 2,
		GroupsOnly = false
	},
	[16] = {
		Name = "Run for your life!",
		Sound = "vo/npc/male01/runforyourlife02.wav",
		Reward = 0,
		Duration = 2,
		GroupsOnly = false
	},
	[17] = {
		Name = "Over here!",
		Sound = "vo/npc/male01/overhere01.wav",
		Reward = 0,
		Duration = 1,
		GroupsOnly = false
	}
}
TAUNTS.PROPS = {
	-- Public (Everyone)
	--  Main taunts, build in.
	[1] = {
		Name = "Stop, your killing me!",
		Sound = "vo/npc/male01/vanswer13.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[2] = {
		Name = "Hey, over here!",
		Sound = "vo/npc/male01/overhere01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[3] = {
		Name = "BOOM headshot!",
		Sound = "taunts/boom_headshot.wav",
		Reward = 5,
		Duration = 2,
		GroupsOnly = false
	},
	[4] = {
		Name = "Go away or...",
		Sound = "taunts/go_away_or_i_shall.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[5] = {
		Name = "I'll be back!",
		Sound = "taunts/ill_be_back.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[6] = {
		Name = "Negative!",
		Sound = "taunts/negative.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[7] = {
		Name = "Doh!",
		Sound = "taunts/doh.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[8] = {
		Name = "You will pay!",
		Sound = "taunts/oh_yea_he_will_pay.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[9] = {
		Name = "Tell me, do you...?",
		Sound = "taunts/ok_i_will_tell_you.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[10] = {
		Name = "Please come again!",
		Sound = "taunts/please_come_again.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[11] = {
		Name = "Thread neutralized!",
		Sound = "taunts/threat_neutralized.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[12] = {
		Name = "What is wrong with you?",
		Sound = "taunts/what_is_wrong_with_you.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[13] = {
		Name = "Whoohoo!",
		Sound = "taunts/woohoo.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[14] = {
		Name = "Chang Wow.",
		Sound = "taunts/props/4.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[15] = {
		Name = "Over 9000!",
		Sound = "taunts/props/6.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[16] = {
		Name = "LEROYYYYYYYYY JENKIIIINS!",
		Sound = "taunts/props/7.wav",
		Reward = 35,
		Duration = 4,
		GroupsOnly = false
	},
	[17] = {
		Name = "This is SPARTAAAAA!",
		Sound = "taunts/props/9.wav",
		Reward = 35,
		Duration = 3,
		GroupsOnly = false
	},
	[18] = {
		Name = "What do you like to play?",
		Sound = "taunts/props/12.wav",
		Reward = 35,
		Duration = 3,
		GroupsOnly = false
	},
	[19] = {
		Name = "Wheeeee!",
		Sound = "taunts/props/14.wav",
		Reward = 10,
		Duration = 3,
		GroupsOnly = false
	},
	[20] = {
		Name = "Snooping as usual!",
		Sound = "taunts/props/15.wav",
		Reward = 10,
		Duration = 3,
		GroupsOnly = false
	},
	[21] = {
		Name = "HA TATATATATA!",
		Sound = "taunts/props/16.wav",
		Reward = 75,
		Duration = 20,
		GroupsOnly = false
	},
	[22] = {
		Name = "Get on the ball!",
		Sound = "taunts/props/18.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[23] = {
		Name = "HONK HONK!",
		Sound = "taunts/props/19.wav",
		Reward = 35,
		Duration = 2,
		GroupsOnly = false
	},
	[24] = {
		Name = "Are you on the ball?",
		Sound = "taunts/props/20.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[25] = {
		Name = "For only 19.99!",
		Sound = "taunts/props/21.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[26] = {
		Name = "Never have to poor...",
		Sound = "taunts/props/22.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[27] = {
		Name = "I guarantee it!",
		Sound = "taunts/props/23.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[28] = {
		Name = "Its new, anti-clean...!",
		Sound = "taunts/props/24.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[29] = {
		Name = "Even your kids can do laundry!",
		Sound = "taunts/props/25.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[30] = {
		Name = "The anti-clean bal!",
		Sound = "taunts/props/27.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[31] = {
		Name = "Laundry just got easier!",
		Sound = "taunts/props/28.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[32] = {
		Name = "Tutututu!",
		Sound = "taunts/props/29.wav",
		Reward = 150,
		Duration = 300,
		GroupsOnly = false
	},
	[33] = {
		Name = "Millitairy forces eliminated!",
		Sound = "taunts/props/30.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[34] = {
		Name = "Bad boys!",
		Sound = "taunts/props/31.mp3",
		Reward = 55,
		Duration = 11,
		GroupsOnly = false
	},
	[35] = {
		Name = "Party!",
		Sound = "taunts/props/32.mp3",
		Reward = 50,
		Duration = 10,
		GroupsOnly = false
	},
	[36] = {
		Name = "Party! (2)",
		Sound = "taunts/props/33.mp3",
		Reward = 35,
		Duration = 7,
		GroupsOnly = false
	},
	[37] = {
		Name = "The one and only!",
		Sound = "taunts/props/34.mp3",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[38] = {
		Name = "HOLOLOOO!",
		Sound = "taunts/props/35.mp3",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	[39] = {
		Name = "Ow shit!",
		Sound = "vo/citadel/br_ohshit.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[40] = {
		Name = "You fool!",
		Sound = "vo/citadel/br_youfool.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[41] = {
		Name = "You need me!",
		Sound = "vo/citadel/br_youneedme.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[42] = {
		Name = "WHOOOHOO, YES!",
		Sound = "vo/coast/odessa/male01/nlo_cheer01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[43] = {
		Name = "WHOOOOHOO!",
		Sound = "vo/coast/odessa/male01/nlo_cheer02.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[44] = {
		Name = "AHHAAAA, YEAH!",
		Sound = "vo/coast/odessa/male01/nlo_cheer03.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[45] = {
		Name = "YEAH, WHOOHAA!",
		Sound = "vo/coast/odessa/male01/nlo_cheer04.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[46] = {
		Name = "Rise and shine Mr. Freeman",
		Sound = "vo/gman_misc/gman_riseshine.wav",
		Reward = 35,
		Duration = 7,
		GroupsOnly = false
	},
	[47] = {
		Name = "Damnit!",
		Sound = "vo/npc/barney/ba_damnit.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[48] = {
		Name = "HAHAHAAAAA!",
		Sound = "vo/npc/barney/ba_laugh01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[49] = {
		Name = "HUHUHAHAAAAA!",
		Sound = "vo/npc/barney/ba_laugh02.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[50] = {
		Name = "HIHU YEAH!",
		Sound = "vo/npc/barney/ba_laugh03.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[51] = {
		Name = "Enough of your mombojumbo",
		Sound = "vo/npc/male01/vanswer01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[52] = {
		Name = "Whoa, Déja vû",
		Sound = "vo/npc/male01/question05.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[53] = {
		Name = "Sometimes, I dream about cheese.",
		Sound = "vo/npc/male01/question06.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[54] = {
		Name = "Don't forget Hawaii!",
		Sound = "vo/npc/male01/answer34.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[55] = {
		Name = "Glas, no kids around.",
		Sound = "vo/npc/male01/question30.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[56] = {
		Name = "This is bullshit!",
		Sound = "vo/npc/male01/question26.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[57] = {
		Name = "Get the hell outta here!",
		Sound = "vo/npc/male01/gethellout.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[58] = {
		Name = "Mad laugh!",
		Sound = "vo/ravenholm/madlaugh04.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[59] = {
		Name = "Piepie for my butthole!",
		Sound = "taunts/fixed/13_fix.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[60] = {
		Name = "Not the bees!",
		Sound = "taunts/fixed/bees_fix.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[61] = {
		Name = "I do not move out of the way!",
		Sound = "taunts/props_extra/dx_idonotmoveout.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[62] = {
		Name = "You just killed an old friend!",
		Sound = "taunts/props_extra/dx_iloominarty.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[63] = {
		Name = "I'm gonna whoop your ass!",
		Sound = "taunts/props_extra/dx_imgonnawoopyourass.wav",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	[64] = {
		Name = "Look at me!",
		Sound = "taunts/props_extra/dx_lookatme.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[65] = {
		Name = "Screaming",
		Sound = "taunts/props_extra/dx_molepeople.wav",
		Reward = 55,
		Duration = 11,
		GroupsOnly = false
	},
	[66] = {
		Name = "The bomb!",
		Sound = "taunts/props_extra/dx_thebomb.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[67] = {
		Name = "A bomb!",
		Sound = "taunts/props_extra/dx_thebomb2.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[68] = {
		Name = "Angry german kiddo",
		Sound = "taunts/props_extra/ext_angry_german_kid.wav",
		Reward = 20,
		Duration = 3,
		GroupsOnly = false
	},
	[69] = {
		Name = "Pong papapong papapa pa!",
		Sound = "taunts/props_extra/ext_blablaahah.wav",
		Reward = 35,
		Duration = 7,
		GroupsOnly = false
	},
	[70] = {
		Name = "Dance music!",
		Sound = "taunts/props_extra/ext_dance_music.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[71] = {
		Name = "Get noscoped!",
		Sound = "taunts/props_extra/ext_get_no_scope.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[72] = {
		Name = "God, I hate you!",
		Sound = "taunts/props_extra/ext_hl1_crackmod_ihateyou.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[73] = {
		Name = "Yes sir, I'll watch your back!",
		Sound = "taunts/props_extra/ext_hl1_crackmod_watchyourrear.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[74] = {
		Name = "Your damn ugly!",
		Sound = "taunts/props_extra/ext_hl1_crackmod_youareugly.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[75] = {
		Name = "Hula dance song",
		Sound = "taunts/props_extra/ext_huladance.mp3",
		Reward = 35,
		Duration = 7,
		GroupsOnly = false
	},
	[76] = {
		Name = "JOHN CENA!",
		Sound = "taunts/props_extra/ext_jojon_sina.wav",
		Reward = 50,
		Duration = 10,
		GroupsOnly = false
	},
	[77] = {
		Name = "DO IT!",
		Sound = "taunts/props_extra/ext_just_do_it_1.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[78] = {
		Name = "JUST DO IT!",
		Sound = "taunts/props_extra/ext_just_do_it_2.wav",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	[79] = {
		Name = "Wepon remix",
		Sound = "taunts/props_extra/ext_wepon.mp3",
		Reward = 55,
		Duration = 11,
		GroupsOnly = false
	},
	[80] = {
		Name = "OOOOH!",
		Sound = "taunts/props_extra/ext_woo.wav",
		Reward = 70,
		Duration = 14,
		GroupsOnly = false
	},
	[81] = {
		Name = "Radio laughing",
		Sound = "taunts/hunters/laugh.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[82] = {
		Name = "Is it me your looking for?",
		Sound = "taunts/props/hello01.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[83] = {
		Name = "Little laugh",
		Sound = "taunts/props/littlegirl01.wav",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	[84] = {
		Name = "Little girl: No!",
		Sound = "taunts/props/littlegirl02.wav",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	[85] = {
		Name = "Okay",
		Sound = "taunts/props/okay01.wav",
		Reward = 10,
		Duration = 1,
		GroupsOnly = false
	},
	--  Added (Custom sounds)
	-- Premium only
	--  Main, build in taunts
	[86] = {
		Name = "Illuminati!",
		Sound = "taunts/props_extra/ext_x_files.wav",
		Reward = 35,
		Duration = 7,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Inspector",
			"Administrator",
			"MVP+",
			"MVP",
			"VIP+"
		}
	},
	-- Added (Custom sounds)
	[87] = {
		Name = "Sandstorm",
		Sound = "taunts/props/sandstorm01.wav",
		Reward = 105,
		Duration = 21,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Inspector",
			"Administrator",
			"MVP+",
			"MVP",
			"VIP+",
			"VIP"
		}
	},
	[88] = {
		Name = "Jorge of the jungle",
		Sound = "taunts/props/jorge_of_the_junge01.wav",
		Reward = 40,
		Duration = 8,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Inspector",
			"Administrator",
			"MVP+",
			"MVP",
			"VIP+",
			"VIP"
		}
	},
	
	-- Added later on...
	[89] = {
		Name = "You're a wizard harry!",
		Sound = "taunts/props/youreawizard01.wav",
		Reward = 105,
		Duration = 86,
		GroupsOnly = false
	},
	[90] = {
		Name = "KARAMBIT DOPPLER!",
		Sound = "taunts/props/karambitdoppler01.wav",
		Reward = 0,
		Duration = 8,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Inspector",
			"Administrator",
			"MVP+",
			"MVP"
		}
	},
	[91] = {
		Name = "Small lone of a million dollars.",
		Sound = "taunts/props/smallloan01.wav",
		Reward = 10,
		Duration = 6,
		GroupsOnly = false
	},
	[92] = {
		Name = "If I ever stop singing I will explode.",
		Sound = "taunts/props/ifieverstop01.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[93] = {
		Name = "Dad, I'm hungry!",
		Sound = "taunts/props/hihungry01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[94] = {
		Name = "Someone kill me!",
		Sound = "taunts/props/somekillme01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[95] = {
		Name = "You're adopted, TUNNN!",
		Sound = "taunts/props/youare01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[96] = {
		Name = "Would you like to see a magic trick?",
		Sound = "taunts/props/wouldyouliketosee01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[97] = {
		Name = "Skateboards",
		Sound = "taunts/props/skateboards01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[98] = {
		Name = "What did you get for your birthday?",
		Sound = "taunts/props/whatdidyouget01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[99] = {
		Name = "Everybody do the flop!",
		Sound = "taunts/props/dotheflop01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[100] = {
		Name = "Don't jump!",
		Sound = "taunts/props/dontjump01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[101] = {
		Name = "Very tall midget",
		Sound = "taunts/props/verytallmiget01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[102] = {
		Name = "Knock knock, who's there?",
		Sound = "taunts/props/thedoor01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[103] = {
		Name = "No idea how to breath",
		Sound = "taunts/props/noidea01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[104] = {
		Name = "Anyone here a doctor?",
		Sound = "taunts/props/doctor01.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[105] = {
		Name = "Here comes the airplane!",
		Sound = "taunts/props/airplane01.wav",
		Reward = 20,
		Duration = 4,
		GroupsOnly = false
	},
	[106] = {
		Name = "The internet is for porn",
		Sound = "taunts/props/internerforporn01.wav",
		Reward = 160,
		Duration = 32,
		GroupsOnly = false
	},
	[107] = {
		Name = "Grab your dick and ...",
		Sound = "taunts/props/internetforporn02.wav",
		Reward = 100,
		Duration = 20,
		GroupsOnly = false
	},
	[108] = {
		Name = "ITS A PRANK!",
		Sound = "taunts/props/itsaprank01.wav",
		Reward = 10,
		Duration = 2,
		GroupsOnly = false
	},
	[109] = {
		Name = "Cat, allahu akbar!",
		Sound = "taunts/props/allahuakbar01.wav",
		Reward = 55,
		Duration = 12,
		GroupsOnly = false
	},
	[110] = {
		Name = "We're not aiming for the truck",
		Sound = "taunts/props/allahuakbar02.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[111] = {
		Name = "Turn the lights off",
		Sound = "taunts/props/turnthelightsof01.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[112] = {
		Name = "No fingering on period",
		Sound = "taunts/props/nofingering01.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[113] = {
		Name = "Knock knock, a mirror!",
		Sound = "taunts/props/mirror01.wav",
		Reward = 15,
		Duration = 3,
		GroupsOnly = false
	},
	[114] = {
		Name = "HEYAY!",
		Sound = "taunts/props/heyay01.wav",
		Reward = 5,
		Duration = 1,
		GroupsOnly = false
	},
	[115] = {
		Name = "Fool who ripped his pants",
		Sound = "taunts/props/rippedpants01.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[116] = {
		Name = "Suck ma wiggly dick",
		Sound = "taunts/props/suckdick01.wav",
		Reward = 30,
		Duration = 6,
		GroupsOnly = false
	},
	[117] = {
		Name = "Tastes like no trump",
		Sound = "taunts/props/liketrump01.wav",
		Reward = 30,
		Duration = 7,
		GroupsOnly = false
	}
}
