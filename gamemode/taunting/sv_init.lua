include("sh_taunts.lua")

-- Adding to the resources list
if SERVER then
	for _, key in pairs(TAUNTS.HUNTERS) do
		resource.AddFile("sound/"..key.Sound)
	end
	for _, key in pairs(TAUNTS.PROPS) do
		resource.AddFile("sound/"..key.Sound)
	end
end


AddCSLuaFile("cl_init.lua")
AddCSLuaFile("sh_taunts.lua")

local PLAYER = FindMetaTable("Player")

util.AddNetworkString("PlaySelectedSound")

hook.Add("PlayerSpawn", "PlayerSpawnTauntMenuEvent", function(Ply)
	if Ply:Team() == TEAM_HUNTERS || Ply:Team() == TEAM_PROPS then
		Ply:SendLua([[notification.AddLegacy("Press 'C' to open the taunt menu", 0, 5)]])
	end
end)

function PLAYER:TauntRandom()
	if self.last_taunt_duration == nil then
		self.last_taunt_duration = TAUNT_DELAY
	end
	if GAMEMODE:InRound() 
		&& self:Alive()
		&& (self:Team() == TEAM_HUNTERS 
			|| self:Team() == TEAM_PROPS)
		&& self.last_taunt_time + self.last_taunt_duration <= CurTime()
		&& #(TAUNTS.HUNTERS) > 1
		&& #(TAUNTS.PROPS) > 1 then
		
		local Taunt = nil
		local OtherTeam = 0
		repeat
			if self:Team() == TEAM_HUNTERS then
				OtherTeam = TEAM_PROPS
				Taunt = table.Random(TAUNTS.HUNTERS)
			else
				OtherTeam = TEAM_HUNTERS
				Taunt = table.Random(TAUNTS.PROPS)
			end
			local Allowed = false
			if Taunt["GroupsOnly"] == true && Taunt["Groups"] then
				for k,v in pairs(Taunt.Groups) do
					if self:IsUserGroup(v) then Allowed = true end
				end
			else Allowed = true end
			if not Allowed then
				Taunt["Sound"] = self.last_taunt
			end
		until Taunt["Sound"] != self.last_taunt
		
		self.last_taunt_time = CurTime()
		self.last_taunt = Taunt["Sound"]
		self.last_taunt_duration = Taunt["Duration"]
		
		self:EmitSound(Taunt["Sound"], 511)
		STATS:AddTaunted(self, Taunt["Name"], 1)
		self:LoadGlobalGroup()
		if Taunt["Reward"] then
			local Reward =  math.floor(tonumber(Taunt["Reward"]) * self.UserGroup["Multiplier"])
			if Reward > 0 && #team.GetPlayers(OtherTeam) > 0 then
				self:AddCurrentPoints(Reward)
				self.last_taunt_reward = Reward
				self:SendLua([[notification.AddLegacy("You've earned ]]..Reward..[[ Points for taunting.", 0, 5)]])
			end
		end
	end
end
function PLAYER:TauntSpecific( sound )
	if self.last_taunt_duration == nil then
		self.last_taunt_duration = TAUNT_DELAY
	end
	if GAMEMODE:InRound() 
	   && self:Alive() 
	   && (self:Team() == TEAM_HUNTERS || self:Team() == TEAM_PROPS)
	   && self.last_taunt_time + self.last_taunt_duration <= CurTime()
	   && #(TAUNTS.HUNTERS) > 1
	   && #(TAUNTS.PROPS) > 1 then
		local OtherTeam = TEAM_PROPS
		local CurrentTauntTable = TAUNTS.HUNTERS
		local Taunt = nil
		if self:Team() == TEAM_PROPS then
			OtherTeam = TEAM_HUNTERS
			CurrentTauntTable = TAUNTS.PROPS
		end
		for _, k in pairs(CurrentTauntTable) do
			if k["Sound"] == sound then
				Taunt = k
			end
		end
		if Taunt then
			local Allowed = false
			if Taunt["GroupsOnly"] == true && Taunt["Groups"] then
				for k,v in pairs(Taunt["Groups"]) do
					if self:IsUserGroup(v) then Allowed = true end
				end
			else Allowed = true end
			
			if not Allowed then
				Taunt = self.last_taunt
			elseif Taunt then
				self.last_taunt_time = CurTime()
				self.last_taunt = Taunt["Sound"]
				self.last_taunt_duration = Taunt["Duration"]
				
				self:EmitSound(Taunt["Sound"], 100)
				STATS:AddTaunted(self, Taunt["Name"], 1)
				self:LoadGlobalGroup()
				if Taunt["Reward"] then
					local Reward = math.floor(tonumber(Taunt["Reward"]) * self.UserGroup["Multiplier"])
					local TimeLeft = GetGlobalFloat( "RoundEndTime") - CurTime()
					if Reward > math.ceil(TimeLeft * 5) then
						Reward = math.floor(TimeLeft * 5)
					end
					if Reward > 0 
					   && #team.GetPlayers(OtherTeam) > 0 then
						self:AddCurrentPoints(Reward)
						self:SendLua([[notification.AddLegacy("You've earned ]]..Reward..[[ Points for taunting.", 0, 5)]])
					end
				end
			end
		end
	end
end
net.Receive("PlaySelectedSound", function(len, Ply) Ply:TauntSpecific( net.ReadString() ) end)