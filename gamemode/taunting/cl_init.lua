include("sh_taunts.lua")

function CreateTauntMenu( page )
	if not page then
		page = 1
	end
	TauntMenu = vgui.Create("DFrame")
	TauntMenu:SetVisible(true)
	TauntMenu:SetTitle("")
	TauntMenu:SetSize(280, 600)
	TauntMenu:SetPos(0,(ScrH()-600)/2)
	TauntMenu:ShowCloseButton(false)
	TauntMenu:MakePopup()
	TauntMenu.Paint = function()
		draw.RoundedBox(4,0,0, TauntMenu:GetWide(), TauntMenu:GetTall(), Color(50,50,50))
	end
	function TauntMenu:OnClose()
		TauntMenu = nil
	end
	
	local pnlScrollList = vgui.Create("DScrollPanel", TauntMenu)
	pnlScrollList:SetSize(250, 525)
	pnlScrollList:SetPos(15, 15)
	
	local pnlItems = vgui.Create("DIconLayout", pnlScrollList)
	pnlItems:SetSize(250, 525)
	pnlItems:SetPos(0,0)
	
	local MY_TEAM_TAUNTS = {}
	if LocalPlayer():Team() == 1 then 
		MY_TEAM_TAUNTS = TAUNTS.HUNTERS
	elseif LocalPlayer():Team() == 2 then
		MY_TEAM_TAUNTS = TAUNTS.PROPS
	end
	local begin = page
	local ending = begin+14
	
	if ending > #MY_TEAM_TAUNTS then
		ending = #MY_TEAM_TAUNTS
	end
	
	-- Bottom buttons
	local btnPrevious = vgui.Create("DButton", TauntMenu)
	btnPrevious:SetPos(15, 550)
	btnPrevious:SetSize(75, 35)
	btnPrevious:SetText("Previous")
	btnPrevious:SetTextColor( Color( 255, 255, 255 ) )
	
	function btnPrevious.DoClick()
		TauntMenu:Close()
		
		
		if begin <= 15 then
			CreateTauntMenu((#MY_TEAM_TAUNTS-14))
		else
			CreateTauntMenu((begin-14))
		end
	end
	function btnPrevious:OnCursorEntered()
		btnPrevious:SetTextColor( Color( 0, 190, 255 ) )
	end
	function btnPrevious:OnCursorExited()
		btnPrevious:SetTextColor( Color( 255, 255, 255 ) )
	end
	function btnPrevious.Paint()
		surface.SetDrawColor(99,99,99)
		surface.DrawRect( 0, 0, btnPrevious:GetWide(), btnPrevious:GetTall() )
	end	
	
	local btnClose = vgui.Create("DButton", TauntMenu)
	btnClose:SetPos(103, 550)
	btnClose:SetSize(75, 35)
	btnClose:SetText("Close")
	function btnClose.DoClick()
		TauntMenu:Close()
	end
	btnClose:SetTextColor( Color( 255, 255, 255 ) )
	function btnClose:OnCursorEntered()
		btnClose:SetTextColor( Color( 0, 190, 255 ) )
	end
	function btnClose:OnCursorExited()
		btnClose:SetTextColor( Color( 255, 255, 255 ) )
	end
	function btnClose.Paint()
		surface.SetDrawColor(99,99,99)
		surface.DrawRect( 0, 0, btnClose:GetWide(), btnClose:GetTall() )
	end
	
	local btnNext = vgui.Create("DButton", TauntMenu)
	btnNext:SetPos(190, 550)
	btnNext:SetSize(75, 35)
	btnNext:SetText("Next")
	function btnNext.DoClick()
		TauntMenu:Close()
		if ending == #MY_TEAM_TAUNTS then
			CreateTauntMenu(1)
		else
			CreateTauntMenu(ending+1)
		end
	end
	btnNext:SetTextColor( Color( 255, 255, 255 ) )
	function btnNext:OnCursorEntered()
		btnNext:SetTextColor( Color( 0, 190, 255 ) )
	end
	function btnNext:OnCursorExited()
		btnNext:SetTextColor( Color( 255, 255, 255 ) )
	end
	function btnNext.Paint()
		surface.SetDrawColor(99,99,99)
		surface.DrawRect( 0, 0, btnNext:GetWide(), btnNext:GetTall() )
	end
	
	
	-- Taunts for loop
	
	for i = begin, ending do
		local v = MY_TEAM_TAUNTS[i]
		
		local ListItem = pnlItems:Add("DPanel")
		ListItem:SetSize(250, 35)
		ListItem:SetCursor("hand")
		ListItem:SetBackgroundColor(team.GetColor(LocalPlayer():Team()))
		ListItem.Taunt = v["Sound"]
		function ListItem:OnCursorEntered()
			function ListItem.Paint()
				local col = ListItem:GetBackgroundColor()
				col.a = 200
				surface.SetDrawColor(col)
				surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
			end
		end
		function ListItem:OnCursorExited()
			function ListItem.Paint()
				local col = ListItem:GetBackgroundColor()
				col.a = 255
				surface.SetDrawColor(col)
				surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
			end
		end
		function ListItem:OnMousePressed( key )
			if key == MOUSE_LEFT then
				PlaySelectedSound(self.Taunt)
				TauntMenu:Close()
			end
		end
		function ListItem.Paint()
			surface.SetDrawColor(team.GetColor(LocalPlayer():Team()))
			surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
		end
		local lblName = vgui.Create("DLabel", ListItem)
		lblName:SetText(v["Name"])
		lblName:SetSize(300, 31)
		lblName:SetPos(4,0)
		lblName:SetBright(1)
	end
end
hook.Add("Think", "ThinkOpenMenuThing", function()
	if LocalPlayer():Alive() && not LocalPlayer():IsTyping() && (LocalPlayer():Team() == TEAM_HUNTERS || LocalPlayer():Team() == TEAM_PROPS) && input.IsKeyDown(KEY_C) then
		if not TauntMenu then
			CreateTauntMenu()
		end
	end
end)
function PlaySelectedSound( sound )
	net.Start("PlaySelectedSound")
	net.WriteString(sound)
	net.SendToServer()
end