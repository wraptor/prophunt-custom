local PLAYER = FindMetaTable("Player")

surface.CreateFont( "LISTEN_UP_FONT", {
	font = "CloseCaption_Bold", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 46,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )
surface.CreateFont( "LISTEN_UP_FONT_SMALL", {
	font = "CloseCaption", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 32,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )

function PLAYER:IsUserGroup(group)
	return (string.lower(self:GetNWString("UserGroup")) == string.lower(group))
end
function PLAYER:IsAdmin()
	if not self:GetNWBool("UserAdmin") || type(self:GetNWBool("UserAdmin")) ~= "boolean" then
		return false
	else
		return self:GetNWBool("UserAdmin")
	end
end
function PLAYER:IsAdmin()
	if not self:GetNWBool("UserSuperAdmin") || type(self:GetNWBool("UserSuperAdmin")) ~= "boolean" then
		return false
	else
		return self:GetNWBool("UserSuperAdmin")
	end
end

net.Receive("ServerPrintPERMMsg", function() chat.AddText(unpack(net.ReadTable())) end)
net.Receive("ServerPrintPERMListenup", function()
	local Listenup = vgui.Create("DFrame")
	local String = net.ReadString()
	Listenup:SetPos(0,100)
	Listenup:SetSize(ScrW(), 123)
	Listenup:ShowCloseButton(false)
	Listenup:SetTitle("");
	function Listenup.Paint()
		draw.RoundedBox(4,0,0, Listenup:GetWide(), Listenup:GetTall(), Color(0,0,0, 200))
		draw.DrawText( "LISTEN UP: ", "LISTEN_UP_FONT", ScrW() * 0.5, 15, Color( 255, 0, 0, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( String, "LISTEN_UP_FONT_SMALL", ScrW() * 0.5, 76, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
	end
	
	timer.Simple(10, function()
		Listenup:Close()
	end)
end)
net.Receive("PlaceRespectThing", function()
	local color = Color( 255, 255, 255, 255 )
	local duration = 7.5
	local fade = 0.5
	local start = CurTime()
	local String = net.ReadString()
	local function drawToScreen()
		local alpha = 255
		local dtime = CurTime() - start

		if dtime > duration then -- Our time has come :'(
			hook.Remove( "HUDPaint", "CSayHelperDraw" )
			return
		end

		if fade - dtime > 0 then -- beginning fade
			alpha = (fade - dtime) / fade -- 0 to 1
			alpha = 1 - alpha -- Reverse
			alpha = alpha * 255
		end

		if duration - dtime < fade then -- ending fade
			alpha = (duration - dtime) / fade -- 0 to 1
			alpha = alpha * 255
		end
		color.a  = alpha

		draw.DrawText( "Press F to pay respect for: "..String, "LISTEN_UP_FONT", ScrW() * 0.5, ScrH() * 0.25, color, TEXT_ALIGN_CENTER )
	end

	hook.Add( "HUDPaint", "CSayHelperDraw", drawToScreen )
end)
hook.Add( "ChatText", "hide_joinleave", function( index, name, text, typ )
	if ( typ == "joinleave" ) then return true end
end )
net.Receive("OpenWebURLByPERM", function()
	local page = net.ReadString()
	gui.OpenURL(page);
end)