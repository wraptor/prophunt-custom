local PLAYER = FindMetaTable("Player")

-- Gets the user's group, synced with all other servers.
function PLAYER:LoadGlobalGroup(steamid)
	if not steamid then steamid = self:SteamID() end
	steamid = MYSQL.DB:escape(steamid)
	MYSQL:Query("SELECT GroupID FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..steamid.."';", function(data)
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(GData)
			self.UserGroup = GData[1]
			self:SetNWString("UserGroup", self.UserGroup.Name)
			
			self.UserGroup.Admin = false
			self:SetNWBool("UserAdmin", false)
			
			self.UserGroup.SuperAdmin = false
			self:SetNWBool("UserSuperAdmin", false)
			
			-- Check if Admin and or SuperAdmin
			HasChildForAdminSuperAdminThing(self, self.UserGroup)
		end)
	end)
end
function HasChildForAdminSuperAdminThing(Ply, Group)
	if Group.Inherits then
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..Group.Inherits.."';", function(data)
			if data && data[1] then
				if data[1].Name:lower() == "admin" then
					Ply.UserGroup.Admin = true
					Ply:SetNWBool("UserAdmin", true)
				elseif data[1].Name:lower() == "superadmin" then
					Ply.UserGroup.SuperAdmin = true
					Ply:SetNWBool("UserSuperAdmin", true)
				end
				HasChildForAdminSuperAdminThing(Ply, data[1])
			end
		end)
	end
end

function PLAYER:IsUserGroup( name )
	return (string.lower(self.UserGroup["Name"]) == string.lower(name))
end
function PLAYER:IsAdmin()
	if self.UserGroup.Admin then
		return self.UserGroup.Admin
	else return false end
end
function PLAYER:IsSuperAdmin()
	if self.UserGroup.SuperAdmin then
		return self.UserGroup.SuperAdmin
	else return false end
end

function PLAYER:GetUserGroup()
	if not self.UserGroup then self:LoadGlobalGroup() end
	return self.UserGroup.Name
end
-- Sets the user's group, synced with all other servers.
function PLAYER:SetGlobalGroup(group)
	MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..self:SteamID().."'", function(r)
		if r && r[1] then
			MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET GroupID='"..group.ID.."' WHERE SteamID='"..self:SteamID().."'")
		else
			MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Players.." (SteamID, Name, Kills, Deaths, Visits, JoinDate, LastJoin, Online, Kicks, Warnings, GroupID, Points) VALUES ('"..self:SteamID().."', 'Unspecified' 0, 0, 0, NOW(), NOW(), 0, 0, 0, '"..group.ID.."', 5000)")
		end
		self:LoadGlobalGroup()
	end)
end
function PLAYER:SetOnline(bool)
	local i = 0
	if bool then i = 1 end
	MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Online="..i..", LastSeenOn='"..MYSQL.InstanceID.."', LastJoin=NOW() WHERE SteamID='"..self:SteamID().."'")
end

-- Create a record for the user.
function PLAYER:CreateRecord()
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Players.." (SteamID, Name, Kills, Deaths, Visits, JoinDate, LastJoin, Online, Kicks, Warnings, GroupID, Points, LastSeenOn) VALUES ('"..self:SteamID().."', 'Unspecified', 0, 0, 1, NOW(), NOW(), 0, 0, 0, 1, 5000, "..MYSQL.InstanceID..");")
end

-- Banning a player (Slamming a hammer on their head aswell, not reall but yeah... :Þ)
function PLAYER:BanPlayer(Banner, Time, Reason)
	if Reason == nil || Reason == "" then Reason = "No reason given." end
	MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..self:SteamID().."' AND Expire >= NOW()", function(data)
		local strTime = "Permanent"
		if Time == 0 then Time = 5256000
		else strTime = os.date("%d/%m/%Y %H:%M:%S", os.time()+(Time*60)) end
		Time = os.time()+(Time*60)
		if not data || not data[1] then
			PERM:CreateBanRecord(self:SteamID(), Banner:SteamID(), Reason, Time)
		else Banner:PrntMSG(Color(255, 120, 120), self:Nick().." is banned already!") return end
		self:Kick("\r\n=============[BANNED]=============\r\n\r\nYou've been banned from the server\r\n\r\n[Reason]\r\n"..Reason.."\r\n\r\n[Expires]\r\n"..strTime.."\r\n\r\n[Banner]\r\n"..Banner:Nick().." ("..Banner:SteamID()..")")
	end)
end
function PLAYER:WarnPlayer(Warner, Reason)
	local WarnerID = nil
	if not Warner then WarnerID = "CONSOLE" else WarnerID = Warner:SteamID() end
	if Reason == nil || Reason == "" then Reason = "No reason given." end
	Reason = string.Replace(MYSQL.DB:escape(Reason), "\\'", "''")
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Warns.." (SteamID, WarnerID, ServerID, Date, Message) VALUES ('"..self:SteamID().."', '"..WarnerID.."', '"..MYSQL.InstanceID.."', CURRENT_TIMESTAMP, '"..Reason.."');")
	PERM:AddWarnings(self:SteamID(), 1)
end
function PLAYER:KickPlayer(Kicker, Reason)
	if Reason == nil || Reason == "" then Reason = "No reason given." end
	self:Kick("\r\n=============[KICKED]=============\r\n\r\nYou've been kicked from the server\r\n\r\n[Reason]\r\n"..Reason.."\r\n\r\n[Kicker]\r\n"..Kicker:Nick().." ("..Kicker:SteamID()..")")
	PERM:CreateKickRecord(self:SteamID(), Kicker:SteamID(), Reason)
	PERM:AddKicks(self:SteamID(), 1)
end
function PLAYER:UpdateName()
	local Nickname = MYSQL.DB:escape(self:Nick())
	MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Name='"..Nickname.."' WHERE SteamID='"..self:SteamID().."';")
end
function PLAYER:PrntMSG(...)
	local Prefix = { Color(255,255,255), "[", Color(255,0,0), "PERM", Color(255,255,255), "] " }
	local Table = { ... }
	for _, k in pairs(Table) do
		table.insert(Prefix, k)
	end
	net.Start("ServerPrintPERMMsg")
	net.WriteTable(Prefix)
	net.Send(self)
end
function PLAYER:SendCustomMessage(Table)
	net.Start("ServerPrintPERMMsg")
	net.WriteTable(Table)
	net.Send(self)
end
function PERM:SweepTrashRecords()
	MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Bans.." WHERE Expire < NOW();")
end

util.AddNetworkString("ServerPrintPERMListenup")

function PLAYER:SendListenup(String)
	net.Start("ServerPrintPERMListenup")
	net.WriteString(String)
	net.Send(self)
end



function PERM:AddKicks( SteamID, Amount )
	SteamID = MYSQL.DB:escape(SteamID)
	MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Kicks = Kicks + "..Amount.." WHERE SteamID='"..SteamID.."';")
end

function PERM:AddWarnings( SteamID, Amount )
	Steamid = MYSQL.DB:escape(SteamID)
	MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Warnings = Warnings + "..Amount.." WHERE SteamID='"..SteamID.."';")
end

-- Kick record (For later administration)
function PERM:CreateKickRecord(SteamID, KickerID, Reason)
	if Reason == nil || Reason == "" then Reason = "No reason given." end
	Reason = MYSQL.DB:escape(Reason)
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Kicks.." (SteamID, KickerID, ServerID, Date, Reason) VALUES ('"..SteamID.."', '"..KickerID.."', "..MYSQL.InstanceID..", NOW(), '"..string.Replace(Reason, "\\'", "''").."');")
end

-- Ban record (For later administration)
function PERM:CreateBanRecord(SteamID, BannerID, Reason, Expire)
	if Reason == nil || Reason == "" then Reason = "No reason given." end
	local Expire = os.date("%d/%m/%Y %H:%M:%S", Expire)
	Reason = MYSQL.DB:escape(Reason)
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Bans.." (SteamID, BannerID, ServerID, Created, Expire, Reason) VALUES ('"..
		SteamID.."', '"..
		BannerID.."', "..
		MYSQL.InstanceID..", NOW(), STR_TO_DATE('"..
		Expire.."', '%d/%m/%Y %H:%i:%S'), '"
		.. string.Replace(Reason, "\\'", "''") .."');")
end

function PERM:IsBanned(SteamID)
	SteamID = MYSQL.DB:escape(SteamID)
	return MYSQL:Exists("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..SteamID.."' AND Expire >= NOW()")
end