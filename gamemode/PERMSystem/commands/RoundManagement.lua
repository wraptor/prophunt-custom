function CMDEndRound(Caller, Arguments)
	if !GAMEMODE:InRound() then
		return
	end
	CallerName = "Mr. Console"
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		CallerName = Caller:Nick()
	end
	GAMEMODE:RoundEndWithResult(TEAM_HUNTERS, "Round ended by "..CallerName)
end

local Command = {}
Command.Commands = {
	"endround"
}
Command.Handler = CMDEndRound
Command.Name = "End round"
Command.Description = "End the currently ongoing round, automatically giving the win to the seekers."
Command.Arguments = 1
Command.Syntax = "endround"
Command.Permission = "PERM_ROUND_END"

PERM:AddCommand(Command)