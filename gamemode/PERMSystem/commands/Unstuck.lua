local m = nil
if not m then
	m = {}
	m[1] = "Don't move, we're trying to free you."
	m[2] = "Couldn't find you a good spot. Make sure you're not aiming at a wall, and try again in 10 seconds!"
	m[3] = "You should be unstuck! You can try again in 10 seconds!"
	m[4] = "You must be alive to use this command!"
	m[5] = "Aim away from any walls, at a spot which is clear!\nProcess begins in 3 seconds!"
	m[6] = "Cooldown period still active! Wait a bit!"
	m[7] = { "Player '", "' used the UnStuck command! Investigate possible misuse." }
	m[8] = "Make sure you're aiming away from all other entities!\nie: Not trying to go through walls"
	m[9] = "You must be a prop/hunter and alive to be stuck."
end


local function SendMessage(Ply, num, Target)
	local Result = "Error, you shouldn't be able to see this message..."
	if Target then
		Result = m[num][1]..Target:Nick()..m[num][2]
	else Result = m[num]
	end
	Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255, 0, 220), "Unstuck", Color(255,255,255), "] "..Result })
end

function FindNewPos( ply )
	local pos = ply:GetShootPos()
	local ang = ply:GetAimVector()
	local forward = ply:GetForward()
	local center = Vector( 0, 0, 30 )
	local realpos = ( (pos + center ) + (forward * 75) )
	
	local chprop = ents.Create( "prop_physics" )
	
		chprop:SetModel( "models/props_c17/oildrum001.mdl" )
		chprop:SetPos( realpos )
		chprop:SetCollisionGroup( COLLISION_GROUP_WEAPON )
		chprop:SetOwner( ply )
		chprop:SetNoDraw( true )
		chprop:DrawShadow( false )
		chprop:DropToFloor()
		chprop:Spawn()
		local p = chprop:GetPhysicsObject()
			if IsValid( p ) then
				p:EnableMotion( false )
			end
	local tracedata = {}
			tracedata.start = pos
			tracedata.endpos = chprop:GetPos()
			tracedata.filter = ply	
	local trace = util.TraceLine(tracedata)
	timer.Create( "CheckUseAblePos", 3, 1, function()
		ply:Freeze( false )
	
		if IsValid( chprop:GetGroundEntity() ) then
			local gent = chprop:GetGroundEntity()
			gent:SetCollisionGroup( COLLISION_GROUP_WEAPON )
				timer.Simple( 3, function()
					gent:SetCollisionGroup( COLLISION_GROUP_NONE )
				end)
		end

		if chprop:IsInWorld() then
			
			if trace.Entity == chprop then
				ply:SetPos( chprop:GetPos() )
				SendMessage( ply, 3 )
			else
				SendMessage( ply, 8 )
			end
		else
		
			SendMessage( ply, 2 )
		end
		chprop:Remove()
	end)
end

function CMDUnStuck(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		if (Caller:Team() == TEAM_PROPS || Caller:Team() == TEAM_HUNTERS) && Caller:Alive() then
			if Caller:GetMoveType() == MOVETYPE_OBSERVER or not Caller:Alive() then return end
			Caller:Freeze( true )
			FindNewPos( Caller )
			SendMessage( Caller, 1 )
			for k,v in pairs( player.GetAll() ) do
				if v:IsAdmin() or v:IsSuperAdmin() or v:IsUserGroup( "Moderator" ) then
					SendMessage( v, 7, Caller )
				end
			end
		else
			SendMessage(Caller, 9)
		end
	else
		print("You're a console and cannot be stuck... at all.")
	end
end
local Command = {}
Command.Commands = {
	"stuck",
	"unstuck"
}
Command.Handler = CMDUnStuck
Command.Name = "Unstuck"
Command.Description = "Tries to free a stuck player."
Command.Arguments = 1
Command.Syntax = "unstuck"
Command.Permission = "PERM_UNSTUCK"

PERM:AddCommand(Command)