function CMDAddPermission(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		-- Player
		if Argument[2] != "*" then
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
				if Group && Group[1] then
					Group = Group[1]
					GroupCanTarget(Caller.UserGroup, Group, Caller, function()
						MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.ID.."' AND Permission='"..MYSQL.DB:escape(Arguments[2]:upper()).."'", function(res)
							if not res || not res[1] then
								PERM:AddPermission(Group.ID, Arguments[2])
								PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Added permission: "..Arguments[2].." to the group: "..Group.Name)
								CreateSyncNotification(Color(60,255,60), "The group ", Color(Group["Red"],Group["Green"],Group["Blue"]), Group["Name"], Color(60,255,60), " has been granted the permission ", Color(255,60,60), Arguments[2], Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), "!")
							else
								Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The group '", Color(Group.Red, Group.Green, Group.Blue), Group.Name, Color(255,60,60), "' already has the permission '", Color(60,255,60), Arguments[2], Color(255,60, 60), "'" })
							end
						end)
					end)
				else
					Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[1], Color(255,60,60), "'" }) 
				end
			end)
		else
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "You cannot give a group all permissions!" }) 
		end
	else
		-- Console
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
			if Group && Group[1] then
				Group = Group[1]
				MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.ID.."' AND Permission='"..MYSQL.DB:escape(Arguments[2]:upper()).."'", function(res)
					if not res || not res[1] then
						PERM:AddPermission(Group.ID, Arguments[2])
						PERM:LogAction(0, "CONSOLE", "Added permission: "..Arguments[2].." to the group: "..Arguments[1])
						CreateSyncNotification(Color(60,255,60), "The group ", Color(Group["Red"],Group["Green"],Group["Blue"]), Group["Name"], Color(60,255,60), " has been granted the permission ", Color(255,60,60), Arguments[2], Color(60,255,60), " by ", Color(255,255,0), "Mr. Console", Color(60,255,60), "!")
						print("[PERM] Added permission '"..Arguments[2].."' to the group '"..Arguments[1].."'")
					else
						print("[PERM] The group '"..Arguments[1].."' already has the permission '"..Arguments[2].."'")
					end
				end)
			else
				print("[PERM] Could not find group '"..Arguments[1].."'") 
			end
		end)
	end
end
local Command = {}
Command.Commands = {
	"addperm",
	"addpermission",
	"addp"
}
Command.Handler = CMDAddPermission
Command.Name = "Add Permission"
Command.Description = "Add a permission to the specified group."
Command.Arguments = 3
Command.Syntax = "addperm <Group (Name)> <Permission> <Max Value (Number)>"
Command.Permission = "PERM_ADD_PERMISSION"

PERM:AddCommand(Command)

function CMDRemovePermission(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		-- Player
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
			if Group && Group[1] then
				Group = Group[1]
				GroupCanTarget(Caller.UserGroup, Group, Caller, function()
					MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.ID.."' AND Permission='"..MYSQL.DB:escape(Arguments[2]:upper()).."'", function(res)
						if res && res[1] then
							PERM:RemovePermission(Group.ID, Arguments[2])
							PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Removed permission: "..Arguments[2].." from the group: "..Group.Name)
							CreateSyncNotification(Color(60,255,60), "The group ", Color(Group["Red"],Group["Green"],Group["Blue"]), Group["Name"], Color(60,255,60), " has been revoked the permission ", Color(255,60,60), Arguments[2], Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), "!")
						else
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The group '", Color(Group.Red, Group.Green, Group.Blue), Group.Name, Color(255,60,60), "' doesn't have the permission '", Color(60,255,60), Arguments[2], Color(255,60, 60), "'" })
						end
					end)
				end)
			else
				Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[1], Color(255,60,60), "'" }) 
			end
		end)
	else
		-- Console
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
			if Group && Group[1] then
				Group = Group[1]
				MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.ID.."' AND Permission='"..MYSQL.DB:escape(Arguments[2]:upper()).."'", function(res)
					if res && res[1] then
						PERM:RemovePermission(Group.ID, Arguments[2])
						PERM:LogAction(0, "CONSOLE", "Removed permission: "..Arguments[2].." from the group: "..Arguments[1])
						CreateSyncNotification(Color(60,255,60), "The group ", Color(Group["Red"],Group["Green"],Group["Blue"]), Group["Name"], Color(60,255,60), " has been revoked the permission ", Color(255,60,60), Arguments[2], Color(60,255,60), " by ", Color(255,255,0), "Mr. Console", Color(60,255,60), "!")
						print("[PERM] Removed permission '"..Arguments[2].."' from the group '"..Arguments[1].."'")
					else
						print("[PERM] The group '"..Arguments[1].."' doesn't have the permission '"..Arguments[2].."'")
					end
				end)
			else
				print("[PERM] Could not find group '"..Arguments[1].."'") 
			end
		end)
	end
end

Command = {}
Command.Commands = {
	"remp",
	"remperm",
	"rempermission",
	"removep",
	"removeperm",
	"removepermission"
}
Command.Handler = CMDRemovePermission
Command.Name = "RemovePermission"
Command.Description = "Remove a permission from the specified group."
Command.Arguments = 3
Command.Syntax = "remperm <Group (Name)> <Permission>"
Command.Permission = "PERM_REMOVE_PERMISSION"

PERM:AddCommand(Command)

function CMDViewPermissions(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
			if Group && Group[1] then
				GetGroupPermissions(Group[1].ID, {}, function(CurrentList)
					if CurrentList[1] then
						for _, k in pairs(CurrentList) do
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] "..k})
						end
					else
						Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The group '", Color(255,255,0), Arguments[1], Color(255,60,60), "' doesn't have any permissions, not even inherited!" }) 
					end
				end)
			else
				Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[1], Color(255,60,60), "'" }) 
			end
		end)
	else
		MYSQL:Query("SELECT ID FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[1]:lower()).."'", function(Group)
			if Group && Group[1] then
				GetGroupPermissions(Group[1].ID, {}, function(CurrentList)
					if CurrentList[1] then
						print("[PERM] Begin permissions list for "..Arguments[1])
						for _, k in pairs(CurrentList) do
							print("[PERM] "..k)
						end
					else
						print("[PERM] The group '"..Arguments[1].."' doesn't have any permissions, not even inherited!") 
					end
				end)
			else
				print("[PERM] Could not find group '"..Arguments[1].."'") 
			end
		end)
	end
end

Command = {}
Command.Commands = {
	"listp",
	"listperms",
	"listpermissions",
	"listrights",
	"viewp",
	"viewperms",
	"viewpermissions"
}
Command.Handler = CMDViewPermissions
Command.Name = "ViewPermissions"
Command.Description = "Shows you all permissions the specified group has"
Command.Arguments = 2
Command.Syntax = "listperm <Group (Name)>"
Command.Permission = "PERM_LIST_PERMISSIONS"

PERM:AddCommand(Command)