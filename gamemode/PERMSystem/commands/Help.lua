function CMDHelp(Caller, Arguments)
	-- Define the command var.
	local Command = nil
	-- Check if it exists.
	for _, k in pairs(PERM.Commands) do
		-- Check if k.Commands is a single command string, if so, validate.
		if type(k.Commands) == "string" && k.Commands:lower() == Arguments[1]:lower() then Command = k
		-- If its a table, search table for the specified command.
		elseif type(k.Commands) == "table" then
			for _, l in pairs(k.Commands) do
				if l:lower() == Arguments[1]:lower() then Command = k end
			end
		end
	end
	
	-- Check if we found the command, if not let the user know
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		if Command then
			Caller:SendCustomMessage({ "[===========================]" })
			Caller:SendCustomMessage({ "Command: "..Command.Name })
			Caller:SendCustomMessage({ "Description: "..Command.Description})
			Caller:SendCustomMessage({ "Arguments: "..Command.Arguments})
			Caller:SendCustomMessage({ "Syntax: "..Command.Syntax })
		else
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] Could not find help for command ", Color(0, 255, 0), Arguments[2], Color(255,255,255), ", not found." })
		end
	else
		if Command then
			print("[===========================]")
			print("Command: "..Command.Name)
			print("Description: "..Command.Description)
			print("Arguments: "..Command.Arguments)
			print("Syntax: "..Command.Syntax)
			print("[===========================]")
		else
			print("[PERM] Could not find help for "..Arguments[1]..", command not found.")
		end
	end
end

local Command = {}
Command.Commands = {
	"help"
}
Command.Handler = CMDHelp
Command.Name = "Help"
Command.Description = "View all information, syntaxes and required arguments from other commands."
Command.Arguments = 2
Command.Syntax = "help <Command>"
Command.Permission = "PERM_HELP"

PERM:AddCommand(Command)

function CMDCommands(Caller, Arguments)
	local CommandList = ""
	for _, Command in pairs(PERM.Commands) do
		CommandList = CommandList..Command.Name..", "
	end
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		Caller:SendCustomMessage({ CommandList })
	else
		print("Command: "..CommandList)
	end
end

Command = {}
Command.Commands = {
	"commands"
}
Command.Handler = CMDCommands
Command.Name = "Commands"
Command.Description = "View all commands."
Command.Arguments = 1
Command.Syntax = "commands"
Command.Permission = "PERM_COMMAND"

PERM:AddCommand(Command)


function CMDOpenSite(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		net.Start("OpenWebURLByPERM")
		net.WriteString("http://prophunt.vlrpgaming.com")
		net.Send(Caller)
	else
		print("You cannot open the website as a console")
	end
end

Command = {}
Command.Commands = {
	"site",
	"website",
	"page",
	"webpage"
}
Command.Handler = CMDOpenSite
Command.Name = "Website"
Command.Description = "View our webiste with all current stats."
Command.Arguments = 1
Command.Syntax = "website"
Command.Permission = "PERM_WEBSITE"

PERM:AddCommand(Command)

function CMDOpenRandomSite(Caller, Arguments)
	local Target = nil
	-- Find the target online.
	for _, tmp in pairs(player.GetAll()) do
		if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
	end
	
	if Target then
		net.Start("OpenWebURLByPERM")
		net.WriteString(table.concat( Arguments, "", 2, #Arguments ))
		net.Send(Target)
	elseif Caller && Caller:IsValid() && Caller:IsPlayer() then
		Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255, 80, 80), "Could not the user." })
	else
		print("Player not found")
	end
end

Command = {}
Command.Commands = {
	"sharesite"
}
Command.Handler = CMDOpenRandomSite
Command.Name = "ShareSite"
Command.Description = "Share a website."
Command.Arguments = 2
Command.Syntax = "sharesite"
Command.Permission = "PERM_SHAREWEBSITE"

PERM:AddCommand(Command)