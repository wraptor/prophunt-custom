util.AddNetworkString("PlaceRespectThing")

local PayingRespect = 0

function CMDPayRespect(Caller, Arguments)
	if PayingRespect < CurTime() + 10 then
		local Target = nil
		for _, tmp in pairs(player.GetAll()) do
			if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
		end
		
		if Target then
			PayingRespect = CurTime()
			for _, tmp in pairs(player.GetAll()) do
				net.Start("PlaceRespectThing")
				net.WriteString(Target:Nick())
				net.Send(tmp)
			end
		elseif Caller && Caller:IsValid() && Caller:IsPlayer() then	
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find user '", Color(255,255,0), Arguments[1], Color(255,60,60), "' not by Name or SteamID" })
		else
			print("[PERM] User doesn't exist! Not found by Name or SteamID.")
		end
	else
		if Caller && Caller:IsValid() && Caller:IsPlayer() then	
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "There's already a pay respect going on." })
		else
			print("[PERM] There's already a pay respect going on.")
		end
	end
end

local Command = {}
Command.Commands = {
	"respect"
}
Command.Handler = CMDPayRespect
Command.Name = "Pay respect"
Command.Description = "Pay respect to the person who just died."
Command.Arguments = 2
Command.Syntax = "respect <Name/SteamID>"
Command.Permission = "PERM_RESPECT"

PERM:AddCommand(Command)