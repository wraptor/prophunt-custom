function CMDSetUser(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		-- Player
		local Target = nil
		
		-- Find the target online.
		for _, tmp in pairs(player.GetAll()) do
			if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
		end
		if Target then
			Target:LoadGlobalGroup()
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[2]:lower()).."';", function(Group)
				if Group && Group[1] then
					Group = Group[1]
					if Target.UserGroup.ID == Group.ID then
						Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(255,60,60), " already is a member of the group '", Color(255,255,0), Group.Name, Color(255,60,60), "'" })
					else
						GroupCanTarget(Caller.UserGroup, Target.UserGroup, Caller, function()
							GroupCanTarget(Caller.UserGroup, Group, Caller, function()
								Target:SetGlobalGroup(Group)
								PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Moved user: "..Target:Nick().." to the group '"..Group.Name.."'")
								CreateSyncNotification(Color(Group.Red, Group.Green, Group.Blue), Target:Nick(), Color(60,255,60), " has been moved to the group ", Color(Group["Red"], Group["Green"], Group["Blue"]), Group.Name, Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), "!")
							end)
						end)
					end
				else
					Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[2], Color(255,60,60), "'" })
				end
			end)
		else
			-- Target not online, let's check the database and find him! If he exists that is.
			SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
			string.Replace(SQLNameOrSteamID, "%", "")
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
				if data && data[1] then
					MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
						if TGroup && TGroup[1] then
							TGroup = TGroup[1]
							MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[2]:lower()).."';", function(Group)
								if Group && Group[1] then
									Group = Group[1]
									if TGroup.ID ~= Group.ID then
										GroupCanTarget(Caller.UserGroup, TGroup, Caller, function()
											GroupCanTarget(Caller.UserGroup, Group, Caller, function()
												MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..data[1].SteamID.."'", function(r)
													if r && r[1] then
														MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET GroupID='"..Group.ID.."' WHERE SteamID='"..data[1].SteamID.."'")
													else
														MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Players.." (SteamID, Kills, Deaths, Visits, JoinDate, LastJoin, Online, Kicks, Warnings, GroupID) VALUES ('"..data[1].SteamID.."', 0, 0, 0, NOW(), NOW(), 0, 0, 0, '"..Group.ID.."')")
													end
												end)
												PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Moved user: "..data[1].Name.." to the group '"..Group.Name.."'")
												CreateSyncNotification(Color(Group.Red, Group.Green, Group.Blue), data[1].Name, Color(60,255,60), " has been moved to the group ", Color(Group.Red, Group.Green, Group.Blue), Group.Name, Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), "!")
											end)
										end)
									else
										Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[2], Color(255,60,60), "'" })
									end
								else
									Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group '", Color(255,255,0), Arguments[2], Color(255,60,60), "'" })
								end
							end)
						else
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target's group '", Color(255,255,0), data[1].GroupID, Color(255,60,60), "'" })
						end
					end)
				else
					Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find user '", Color(255,255,0), Arguments[1], Color(255,60,60), "' not by Name or SteamID" })
				end
			end)
		end
	else
		-- Console
		local Target = nil
		
		-- Find the target online.
		for _, tmp in pairs(player.GetAll()) do
			if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
		end
		if Target then
			Target:LoadGlobalGroup()
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[2]:lower()).."'", function(Group)
				if Group && Group[1] then
					Group = Group[1]
					if Target.UserGroup.ID ~= Group.ID then
						Target:SetGlobalGroup(Group)
						PERM:LogAction(0, "CONSOLE", "Moved user: "..Target:Nick().." to the group '"..Group.Name.."'")
						CreateSyncNotification(Color(Group.Red, Group.Green, Group.Blue), Target:Nick(), Color(60,255,60), " has been moved to the group ", Color(Group["Red"], Group["Green"], Group["Blue"]), Group.Name, Color(60,255,60), " by ", Color(255,255,20), "Mr. Console", Color(60,255,60), "!")
					else
						print("[PERM] User "..Target:Nick().." already is a member of the group '"..Group.Name.."'")
					end
				else
					print("[PERM] Could not find group '"..Arguments[2].."'")
				end
			end)
		else
			-- Target not online, let's check the database and find him! If he exists that is.
			SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
			string.Replace(SQLNameOrSteamID, "%", "")
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
				if data && data[1] then
					MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE LOWER(Name)='"..MYSQL.DB:escape(Arguments[2]:lower()).."';", function(Group)
						if Group && Group[1] then
							Group = Group[1]
							if data[1].GroupID ~= Group.ID then
								MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..data[1].SteamID.."'", function(r)
									if r && r[1] then
										MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET GroupID='"..Group.ID.."' WHERE SteamID='"..data[1].SteamID.."'")
									else
										MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Players.." (SteamID, Kills, Deaths, Visits, JoinDate, LastJoin, Online, Kicks, Warnings, GroupID) VALUES ('"..data[1].SteamID.."', 0, 0, 0, NOW(), NOW(), 0, 0, 0, '"..Group.ID.."')")
									end
								end)
								PERM:LogAction(0, "CONSOLE", "Moved user: "..data[1].Name.." to the group '"..Group.Name.."'")
								CreateSyncNotification(Color(Group.Red, Group.Green, Group.Blue), data[1].Name, Color(60,255,60), " has been moved to the group ", Color(Group.Red, Group.Green, Group.Blue), Group.Name, Color(60,255,60), " by ", Color(255,255,20), "Mr. Console", Color(60,255,60), "!")
								print("[PERM] User "..data[1].Name.." has been moved to the group '"..Group.Name.."'")
							else
								print("[PERM] User "..data[1].Name.." already is a member of the group '"..Group.Name.."'")
							end
						else
							print("[PERM] Could not find the group '"..Arguments[2].."'")
						end
					end)
				else
					print("[PERM] Could not find the user '"..Arguments[1].."' not by Name or SteamID.")
				end
			end)
		end
	end
end

local Command = {}
Command.Commands = {
	"setuser",
	"setu",
	"setusergroup",
	"setrank"
}
Command.Handler = CMDSetUser
Command.Name = "SetUser"
Command.Description = "Sets the specified user's group to the one you specified aswell."
Command.Arguments = 3
Command.Syntax = "setuser <Name/SteamID> <Group (Name)>"
Command.Permission = "PERM_SET_USER"

PERM:AddCommand(Command)