function CMDWarnUser(Caller, Arguments)
	// PLAYER:WarnPlayer(Warner, Reason)
	local Target = nil
	
	-- Find the target online.
	for _, tmp in pairs(player.GetAll()) do
		if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
	end
	
	local Reason = string.Replace(table.concat(Arguments, " ", 2), " ' ", "'")
	if Reason == "" || Reason == " " then Reason = "No reason given." end
	
	if Target then
		Target:LoadGlobalGroup()
		-- Create timer to give the LoadGlobalGroup time to finish and check if allowed to ban.
		timer.Simple(0.2, function()
			if Caller && Caller:IsValid() && Caller:IsPlayer() then
				GroupCanTarget(Caller.UserGroup, Target.UserGroup, Caller, function()
					Target:WarnPlayer(Caller, Reason)
					PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Warned user: "..Target:Nick().." for: "..Reason)
					CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been warned for: ", Color(255,100,100), Reason, Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), ".")
				end)
			else
				Target:WarnPlayer(nil, Reason)
				PERM:LogAction(0, "Console", "Warned user: "..Target:Nick().." for: "..Reason)
				CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been warned for: ", Color(255,100,100), Reason, Color(60,255,60), " by ", Color(255,255,60), "Mr. Console", Color(60,255,60), ".")
				print("[PERM] Successfully warned "..Target:Nick().." for: "..Reason)
			end
		end)
	else
		-- Target not online, let's check the database and find him! If he exists that is.
		SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
		string.Replace(SQLNameOrSteamID, "%", "")
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
			if data && data[1] then
				MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
					if TGroup && TGroup[1] then
						TGroup = TGroup[1]
						if Caller && Caller:IsValid() && Caller:IsPlayer() then
							GroupCanTarget(Caller.UserGroup, TGroup, Caller, function()
								local QReason = string.Replace(MYSQL.DB:escape(Reason), "\\'", "''")
								MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Warns.." (SteamID, WarnerID, ServerID, Message) VALUES ('"..data[1].SteamID.."', '"..Caller:SteamID().."', '"..MYSQL.InstanceID.."', '"..QReason.."');")
								PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Warned user: "..data[1].Name.." for: "..Reason)
								CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been warned for: ", Color(255,100,100), Reason, Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), ".")
							end)
						else
							local QReason = string.Replace(MYSQL.DB:escape(Reason), "\\'", "''")
							MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Warns.." (SteamID, WarnerID, ServerID, Message) VALUES ('"..data[1].SteamID.."', 'CONSOLE', '"..MYSQL.InstanceID.."', '"..QReason.."');")
							PERM:LogAction(0, "Console", "Warned user: "..data[1].Name.." for: "..Reason)
							CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been warned for: ", Color(255,100,100), Reason, Color(60,255,60), " by ", Color(255,255,60), "Mr Console", Color(60,255,60), ".")
							print("[PERM] Successfully warned "..data[1].Name.." for: "..Reason)
						end
						PERM:AddWarnings(data[1].SteamID, 1)
					else
						if Caller && Caller:IsValid() && Caller:IsPlayer() then
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target's group '", Color(255,255,0), data[1].GroupID, Color(255,60,60), "'" })
						else
							print("[PERM] Could not find target's group '"..data[1].GroupID.."'")
						end
					end
				end)
			else
				if Caller && Caller:IsValid() && Caller:IsPlayer() then
					Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target '"..Arguments[1].."'" })
				else
					print("[PERM] User "..Arguments[1].." not found, not even by SteamID")
				end
			end
		end)
	end
end
local Command = {}
Command.Commands = {
	"warn",
	"warnu",
	"warnuser"
}
Command.Handler = CMDWarnUser
Command.Name = "Warn"
Command.Description = "Gives the Target user a warning with the specified reason."
Command.Arguments = 3
Command.Syntax = "warn <Name/SteamID> <Reason>"
Command.Permission = "PERM_WARN"

PERM:AddCommand(Command)

function CMDUnwarnUser(Caller, Arguments)
	if not tonumber(Arguments[2]) then
		if Caller && Caller:IsValid() && Caller:IsPlayer() then
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), " Warn ID must be a number!" })
		else
			print("[PERM] Warn ID must be a number!")
		end
		return	
	end
	-- Let's check the database and find him! If he exists that is.
	SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
	string.Replace(SQLNameOrSteamID, "%", "")
	MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
		if data && data[1] then
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Warns.." WHERE WarnID='"..tonumber(Arguments[2]).."' AND SteamID='"..data[1].SteamID.."';", function(WarnInfo)
				if WarnInfo && WarnInfo[1] then
					if WarnInfo[1].WarnerID == "CONSOLE" then
						if Caller && Caller:IsValid() && Caller:IsPlayer() then
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "You cannot remove warns placed by the console!" })
						else
							MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Warns.." WHERE WarnID='"..tonumber(Arguments[2]).."' AND SteamID='"..data[1].SteamID.."'")
							PERM:AddWarnings(data[1].SteamID, - 1)
							PERM:LogAction(0, "Console", "Removed warn #"..Arguments[2].." for the user: "..data[1].Name)							
							MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
								if TGroup && TGroup[1] then
									TGroup = TGroup[1]
									CreateSyncNotification(Color(255,255,60), "Mr. Console", Color(60,255,60), " removed warning #"..Arguments[2].." from the user: ", Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), ".")
								end
							end)
							print("[PERM] Successfully removed warn #"..Arguments[2].. " from the user '"..data[1].Name.."'")
						end
					else
						MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID IN (SELECT GroupID FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..WarnInfo[1].WarnerID.."')", function(WGroup)
							if WGroup && WGroup[1] then
								MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
									if TGroup && TGroup[1] then
										TGroup = TGroup[1]
										
										if Caller && Caller:IsValid() && Caller:IsPlayer() then
											if Caller:SteamID() == WarnInfo[1].WarnerID then
												MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Warns.." WHERE WarnID='"..tonumber(Arguments[2]).."' AND SteamID='"..data[1].SteamID.."'")
												PERM:AddWarnings(data[1].SteamID, - 1)
												PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Removed warn #"..Arguments[2].." from the user: "..data[1].Name)
												CreateSyncNotification(Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), " removed warning #"..Arguments[2].." from the user: ", Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), ".")
											else
												GroupCanTarget(Caller.UserGroup, WGroup[1], Caller, function()
													MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Warns.." WHERE WarnID='"..tonumber(Arguments[2]).."' AND SteamID='"..data[1].SteamID.."'")
													PERM:AddWarnings(data[1].SteamID, - 1)
													PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Removed warn #"..Arguments[2].." from the user: "..data[1].Name)
													CreateSyncNotification(Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), " removed warning #"..Arguments[2].." from the user: ", Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), ".")
												end)
											end
										else
											PERM:LogAction(0, "Console", "Removed warn #"..Arguments[2].." from the user: "..data[1].Name)
											CreateSyncNotification(Color(255,255,60), "Mr. Console", Color(60,255,60), " removed warning #"..Arguments[2].." from the user: ", Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), ".")
											print("[PERM] Successfully removed warn #"..Arguments[2].. " from the user '"..data[1].Name.."'")
										end
									else
										if Caller && Caller:IsValid() && Caller:IsPlayer() then
											Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target's group '", Color(255,255,0), data[1].GroupID, Color(255,60,60), "'" })
										else
											print("[PERM] Could not find target's group '"..data[1].GroupID.."'")
										end
									end
								end)
							else
								if Caller && Caller:IsValid() && Caller:IsPlayer() then
									Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find warner's group '", Color(255,255,0), data[1].GroupID, Color(255,60,60), "'" })
								else
									print("[PERM] Could not find warner's group.")
								end
							end
						end)
					end
				else
					if Caller && Caller:IsValid() && Caller:IsPlayer() then
						Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Specified warn not found, check User/WarnID combination" })
					else
						print("[PERM] Specified warn not found, check User/WarnID combination.")
					end
				end
			end)
		else
			if Caller && Caller:IsValid() && Caller:IsPlayer() then
				Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target '"..Arguments[1].."'" })
			else
				print("[PERM] User "..Arguments[1].." not found, not even by SteamID")
			end
		end
	end)
end
local Command = {}
Command.Commands = {
	"unwarn",
	"unwarnu",
	"unwarnuser"
}
Command.Handler = CMDUnwarnUser
Command.Name = "Unwarn"
Command.Description = "Removes a warn from the specified Target, by ID."
Command.Arguments = 3
Command.Syntax = "unwarn <Name/SteamID> <WarnID>"
Command.Permission = "PERM_WARN_UNDO"

PERM:AddCommand(Command)