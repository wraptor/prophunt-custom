function CMDBanUser(Caller, Arguments, MaxTime) -- 1 = Target, 2 = Time, 3, +1, +2, +3,... = Reason.
	local Target = nil
	-- Find the target online.
	for _, tmp in pairs(player.GetAll()) do
		if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
	end
	
	if Caller && Caller:IsValid() && Caller:IsPlayer() then	
		-- Check max time
		local MaxTime = nil
		local tmpGName = Caller.UserGroup.Name:lower()
		if MaxxTime 
			&& type(tonumber(MaxxTime)) == "number" 
			&& MaxxTime > 0 then
			MaxxTime = MaxTime
		elseif not MaxxTime then
			MaxTime = 7884000
		else Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Max ban-time not specified, please contact a higher rank! Canceling..." }) return end
		-- Done I guess :�	
		
		if type(tonumber(Arguments[2])) ~= "number" then Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The specified Time must be a number!" }) return end
		
		-- To number, because I'm a Object Oriented dude :�
		Arguments[2] = tonumber(Arguments[2])
		
		if Arguments[2] == 0
			&& MaxTime > 0 then
			Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "You are not permitted to permanently ban, please try a number higher than 0." })
			return
		elseif Arguments[2] > 0 && MaxTime > 0 && Arguments[2] > MaxTime then
			Arguments[2] = MaxTime
		end
		
		local Reason = string.Replace(table.concat(Arguments, " ", 3), " ' ", "'")
		
		if Target then
			Target:LoadGlobalGroup()
			-- Create timer to give the LoadGlobalGroup time to finish and check if allowed to ban.
			timer.Simple(0.2, function()
				GroupCanTarget(Caller.UserGroup, Target.UserGroup, Caller, function()
					Target:BanPlayer(Caller, Arguments[2], Reason)
					PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Banned user: "..Target:Nick().." for "..Arguments[2].." minutes, reason: "..Reason.."")
					CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been banned for ", Color(0,177,255), Arguments[2], Color(60,255,60), " minutes by ", Color(Caller.UserGroup.Red,Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), " reason: ", Color(200,200,200), Reason)
				end)
			end)
		else
			-- Target not online, let's check the database and find him! If he exists that is.
			SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
			string.Replace(SQLNameOrSteamID, "%", "")
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
				if data && data[1] then
					MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
						if TGroup && TGroup[1] then
							TGroup = TGroup[1]
							GroupCanTarget(Caller.UserGroup, TGroup, Caller, function()
								
								-- Custom ban sytem :�
								MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()", function(res)
									if not res || not res[1] then
										if Arguments[2] == 0 then Arguments[2] = 5256000 end
										local Time = os.time()+(Arguments[2]*60)
										PERM:CreateBanRecord(data[1].SteamID, Caller:SteamID(), Reason, Time)
										PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Banned user: "..data[1].Name.." ("..data[1].SteamID..") for "..Arguments[2].." minutes, reason: "..Reason.."")
										CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been banned for ", Color(0,177,255), Arguments[2].."", Color(60,255,60), " minutes by ", Color(Caller.UserGroup.Red,Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), " reason: ", Color(200,200,200), Reason)
									else Caller:PrntMSG(Color(255, 120, 120), data[1].Name.." is banned already!") return end
								end)
							end)
						else
							Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find target's group '", Color(255,255,0), data[1].GroupID, Color(255,60,60), "'" })
						end
					end)
				else
					Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find user '", Color(255,255,0), Arguments[1], Color(255,60,60), "' not by Name or SteamID" })
				end
			end)
		end
	else
		-- Console
		if type(tonumber(Arguments[2])) ~= "number" then Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The specified Time must be a number!" }) return end
		
		-- To number, because I'm a Object Oriented dude :�
		Arguments[2] = tonumber(Arguments[2])
		
		local Time = Arguments[2]
		
		local Reason = string.Replace(table.concat(Arguments, " ", 3), " ' ", "'")
		
		if Target then
			Target:LoadGlobalGroup()
			-- Create timer to give the LoadGlobalGroup time to finish and check if allowed to ban.
			timer.Simple(0.2, function()
				MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..Target:SteamID().."' AND Expire >= NOW()", function(data)
					local strTime = "Permanent"
					if Time == 0 then Time = 5256000
					else strTime = os.date("%d/%m/%Y %H:%M:%S", os.time()+(Time*60)) end
					Time = os.time()+(Time*60)
					if not data || not data[1] then
						PERM:CreateBanRecord(Target:SteamID(), "Console", Reason, Time)
						print("[PERM] "..Target:Nick().." has been logged as banned, kicking...")
					else print("[PERM] "..Target:Nick().." is banned already! Kicking him because glitch xD")  return end
					Target:Kick("\r\n=============[BANNED]=============\r\n\r\nYou've been banned from the server\r\n\r\n[Reason]\r\n"..Reason.."\r\n\r\n[Expires]\r\n"..strTime.."\r\n\r\n[Banner]\r\nMr. Console (The real one, cuz no SteamID :�)")
				end)
				PERM:LogAction(0, "Console", "Banned user: "..Target:Nick().." for "..Arguments[2].." minutes, reason: "..Reason.."")
				CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been banned for ", Color(0,177,255), Arguments[2], Color(60,255,60), " minutes by ", Color(255,255,20), "Mr. Console", Color(60,255,60), " reason: ", Color(200,200,200), Reason)
			end)
		else
			-- Target not online, let's check the database and find him! If he exists that is.
			SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
			string.Replace(SQLNameOrSteamID, "%", "")
			MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
				if data && data[1] then
					MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
						if TGroup && TGroup[1] then
							TGroup = TGroup[1]
							
							-- Custom ban sytem :�
							MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()", function(res)
								if not res || not res[1] then
									if Arguments[2] == 0 then Arguments[2] = 5256000 end
									local Time = os.time()+(Arguments[2]*60)
									PERM:CreateBanRecord(data[1].SteamID, "Console", Reason, Time)
									PERM:LogAction(0, "CONSOLE", "Banned user: "..data[1].Name.." ("..data[1].SteamID..") for "..Arguments[2].." minutes, reason: "..Reason.."")
									CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been banned for ", Color(0,177,255), Arguments[2].."", Color(60,255,60), " minutes by ", Color(255,255,20), "Mr. Console", Color(60,255,60), " reason: ", Color(200,200,200), Reason)
									print("[PERM] Ban successfull! Other servers will check for bans soon!")
								else print("[PERM] "..data[1].Name.." is banned already!") return end
							end)
						else
							print("[PERM] Could not find target's group '"..data[1].GroupID.."'")
						end
					end)
				else
					print("[PERM] Could not find user '"..Arguments[1].."' not by Name or SteamID")
				end
			end)
		end
	end
end

local Command = {}
Command.Commands = {
	"ban",
	"banuser"
}
Command.Handler = CMDBanUser
Command.Name = "Ban"
Command.Description = "Bans the specified Target from all syncronized servers and kicks them if their online on one of them."
Command.Arguments = 4
Command.Syntax = "ban <Name/SteamID> <Time (In Minutes)> <Reason>"
Command.Permission = "PERM_BAN"

PERM:AddCommand(Command)


function CMDUnbanUser(Caller, Arguments)
	SQLNameOrSteamID = MYSQL.DB:escape(Arguments[1])
	string.Replace(SQLNameOrSteamID, "%", "")
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		-- Player
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
			if data && data[1] then
				MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()", function(deta)
					if deta && deta[1] then
						MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
							if TGroup && TGroup[1] then
								TGroup = TGroup[1]
								GroupCanTarget(Caller.UserGroup, TGroup, Caller, function()
									MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()")
									PERM:LogAction(Caller.UserGroup.ID, Caller:Nick(), "Unbanned user: "..data[1].Name.." ("..data[1].SteamID..")")
									CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been unbanned by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), ".")
								end)
							else
								Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find group for the user '", Color(255,255,0), data[1].Name, Color(255,60,60), "'" })
							end
						end)
					else
						Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "The user '", Color(255,255,0), data[1].Name, Color(255,60,60), "' isn't banned." })
					end
				end)
			else
				Caller:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,60,60), "Could not find user '", Color(255,255,0), Arguments[1], Color(255,60,60), "' not by Name or SteamID" })
			end
		end)
	else
		-- Console
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Players.." WHERE LOWER(Name) like '%"..SQLNameOrSteamID:lower().."%' OR SteamID='"..SQLNameOrSteamID.."';", function(data)
			if data && data[1] then
				MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()", function(deta)
					if deta && deta[1] then
						MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..data[1].SteamID.."' AND Expire >= NOW()")
						PERM:LogAction(0, "CONSOLE", "Unbanned user: "..data[1].Name.." ("..data[1].SteamID..")")
						print("[PERM] Ban successfull! Other servers will check for bans soon!")
						MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..data[1].GroupID.."';", function(TGroup)
							if TGroup && TGroup[1] then
								TGroup = TGroup[1]
								CreateSyncNotification(Color(TGroup.Red, TGroup.Green, TGroup.Blue), data[1].Name, Color(60,255,60), " has been unbanned by ", Color(255,255,20), "Mr. Console", Color(60,255,60), ".")
							end
						end)
					else
						print("[PERM] The user '"..data[1].Name.."' isn't banned.")
					end
				end)
			else 
				print("[PERM] User doesn't exist! Not found by Name or SteamID.")
			end
		end)
	end
end

Command = {}
Command.Commands = {
	"unban",
	"unbanuser",
}
Command.Handler = CMDUnbanUser
Command.Name = "Unban"
Command.Description = "Unbans the specified Target from all syncronized servers."
Command.Arguments = 2
Command.Syntax = "unban <Name/SteamID>"
Command.Permission = "PERM_BAN_UNDO"

PERM:AddCommand(Command)