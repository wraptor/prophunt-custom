function CMDListenup(Caller, Arguments)
	local Message = string.Replace(table.concat(Arguments, " "), " ' ", "'")
	CreateSyncListenup(unpack(Arguments))
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Broadcasted: "..Message)
	else
		PERM:LogAction(0, "Console", "Broadcasted: "..Message)
	end
end

local Command = {}
Command.Commands = {
	"listenup",
	"broadcast",
	"listen",
	"csay"
}
Command.Handler = CMDListenup
Command.Name = "ListenUp"
Command.Description = "Display a big message in the center of their screen"
Command.Arguments = 2
Command.Syntax = "listen <Message>"
Command.Permission = "PERM_BROADCAST"

PERM:AddCommand(Command)