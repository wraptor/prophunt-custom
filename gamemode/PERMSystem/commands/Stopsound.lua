function CMDStopSound(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then	
		Caller:StopSound(Caller.last_taunt)
		Caller:RemoveCurrentPoints(Caller.last_taunt_reward)
	else
		print("[PERM] You cannot do stopsound...")
	end
end

local Command = {}
Command.Commands = {
	"stopsound"
}
Command.Handler = CMDStopSound
Command.Name = "Stopsound"
Command.Description = "Stops all local sounds. Only for you."
Command.Arguments = 1
Command.Syntax = "stopsound"
Command.Permission = "PERM_STOPSOUND"

PERM:AddCommand(Command)