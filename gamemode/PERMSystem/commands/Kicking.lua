function CMDKickUser(Caller, Arguments)
	if Caller && Caller:IsValid() && Caller:IsPlayer() then
		-- Player
		local Target = nil
		
		-- Find the target online.
		for _, tmp in pairs(player.GetAll()) do
			if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
		end
		if Target then
			GroupCanTarget(Caller.UserGroup, Target.UserGroup, Caller, function()
				table.remove(Arguments, 1)
				local Reason = table.concat(Arguments, " ")
				if Reason == "" then Reason = "No reason given." end
				Target:KickPlayer(Caller, Reason)
				PERM:LogAction(Caller.UserGroup.ID, Caller:SteamID(), "Kicked the user: "..Target:Nick().." from server #".. ( MYSQL.InstanceID - 1 )..". Reason: "..Reason )
				CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been kicked from server ", Color(255,255,255),"#"..MYSQL.InstanceID, Color(60,255,60), " by ", Color(Caller.UserGroup.Red, Caller.UserGroup.Green, Caller.UserGroup.Blue), Caller:Nick(), Color(60,255,60), ". Reason: ", Color(200,200,200), Reason)
			end)
		else
			Caller:SendCustomMessage({  Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] Could not find player ", Color(60,255,60), Arguments[1], ", is he on this server?" })
		end
	else
		-- Console
		local Target = nil
		
		-- Find the target online.
		for _, tmp in pairs(player.GetAll()) do
			if string.find(tmp:Nick():lower(), Arguments[1]:lower()) || tmp:SteamID() == Arguments[1]:upper() then Target = tmp break end
		end
		if Target then
			table.remove(Arguments, 1)
			local Reason = table.concat(Arguments, " ")
			Reason = string.Replace(Reason, " ' ", "'")
			if Reason == "" then Reason = "No reason given." end
			
			-- General Kick function consolefied
			Target:Kick("\r\n=============[KICKED]=============\r\n\r\nYou've been kicked from the server\r\n\r\n[Reason]\r\n"..Reason.."\r\n\r\n[Kicker]\r\nMr. Console! (REAL Console, No SteamID)")
			PERM:AddKicks(Target:SteamID(), 1)
			PERM:CreateKickRecord(Target:SteamID(), "CONSOLE", Reason)
			
			PERM:LogAction(0, "CONSOLE", "Kicked the user: "..Target:Nick().." from server #".. ( MYSQL.InstanceID - 1 )..". Reason: "..Reason )
			CreateSyncNotification(Color(Target.UserGroup.Red, Target.UserGroup.Green, Target.UserGroup.Blue), Target:Nick(), Color(60,255,60), " has been kicked from server ", Color(255,255,255),"#"..MYSQL.InstanceID, Color(60,255,60), " by ", Color(255,255,20), "Mr. Console", Color(60,255,60), ". Reason: ", Color(200,200,200), Reason)
		else
			print("[PERM] The user "..Arguments[1].." could not be found, is he on this server?")
		end
	end
end

local Command = {}
Command.Commands = {
	"kick",
	"kickuser",
	"kicku"
}
Command.Handler = CMDKickUser
Command.Name = "Kick"
Command.Description = "Kick's a user from the server. (Disconnecting him and showing the specified message)"
Command.Arguments = 3
Command.Syntax = "kick <Name/SteamID> <Reason>"
Command.Permission = "PERM_KICK"

PERM:AddCommand(Command)