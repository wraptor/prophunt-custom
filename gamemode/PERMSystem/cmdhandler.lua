-- Network strings
util.AddNetworkString("ServerPrintPERMMsg")

function PERM:AddCommand(Command)
	table.insert(PERM.Commands, Command)
end

-- Registrate commands
if not PERM.Commands then
	PERM.Commands = {}
	for _, f in pairs(file.Find( "gamemodes/prop_hunt/gamemode/PERMSystem/commands/*.lua", "MOD" )) do
		include("commands/"..f)
	end
end




function HandleCommands(Ply, Arguments)
	local Message = { Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] " }
	-- Check if player, if not it'll be a console, I guess.
	if Ply && Ply:IsValid() && Ply:IsPlayer() then
		-- Load the users group (To be sure I didn't tamper with the DB :Þ)
		Ply:LoadGlobalGroup()
		
		-- Define the command var.
		local Command = nil
		-- Check if it exists.
		for _, k in pairs(PERM.Commands) do
			-- Check if k.Commands is a single command string, if so, validate.
			if type(k.Commands) == "string" && k.Commands:lower() == Arguments[1]:lower() then Command = k
			-- If its a table, search table for the specified command.
			elseif type(k.Commands) == "table" then
				for _, l in pairs(k.Commands) do
					if l:lower() == Arguments[1]:lower() then Command = k end
				end
			end
		end
		
		-- Check if we found the command, if not let the user know
		if Command then
			-- Do you have permissions?
			local ID = 0
			if not Ply.UserGroup.ID then ID = 1 else ID = Ply.UserGroup.ID end
			CommandCheckPermissionAndContinue(Ply, ID, Command, Arguments)
		else
			table.Add(Message, { Color(255,0,0), "Unknown command." })
			Ply:SendCustomMessage(Message)
		end
	else
		-- You're a console! My man, full controll b1tch!
		
		-- Define the command var.
		local Command = nil
		-- Check if it exists.
		for _, k in pairs(PERM.Commands) do
			-- Check if k.Commands is a single command string, if so, validate.
			if type(k.Commands) == "string" && k.Commands:lower() == Arguments[1]:lower() then Command = k
			-- If its a table, search table for the specified command.
			elseif type(k.Commands) == "table" then
				for _, l in pairs(k.Commands) do
					if l:lower() == Arguments[1]:lower() then Command = k end
				end
			end
		end
		
		-- Check if we found the command, if not let the user know
		if Command then
			if #Arguments >= Command.Arguments then
				table.remove(Arguments, 1)
				Command.Handler(nil, Arguments)
			else print("[PERM] Wrong syntax, try: perm "..Command.Syntax) end
		else
			print("[PERM] Command not found!")
		end
	end
end
concommand.Add("perm", function(Ply, cmd, args) HandleCommands(Ply, args) end)

-- Check for permissions and continue with the command
function CommandCheckPermissionAndContinue(Ply, Group, Command, Arguments)
	if Group then
		MYSQL:Query("SELECT Inherits FROM "..MYSQL.Tables.Groups.." WHERE ID='"..Group.."';", function(data)
			if data && data[1] then
				MYSQL:Query("SELECT MaxVal FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.."' AND (Permission='"..MYSQL.DB:escape(Command.Permission).."' OR Permission='*');", function(res)
					if not res || not res[1] then
						CommandCheckPermissionAndContinue(Ply, data[1].Inherits, Command, Arguments)
					else
						-- Well, you've got through all security checks!
						--   Do the rest.
						if #Arguments >= Command.Arguments then
							table.remove(Arguments, 1)
							Command.Handler(Ply, Arguments, res[1].MaxValue)
						else Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,100,100), "Wrong syntax, try: ", Color(0, 220, 50), "!"..Command.Syntax }) end
					end
				end)
			else Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,0,0), "Access denied." }) end
		end)
	else Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,0,0), "Access denied." }) end
end

-- Check if can target group
function GroupCanTarget(Group, Target, Ply, callbackfunction)
	if Group.Inherits then
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Groups.." WHERE ID='"..Group.Inherits.."';", function(data)
			if data && data[1] then
				if data[1].ID == Target.ID then
					callbackfunction()
				else
					GroupCanTarget(data[1], Target, Ply, callbackfunction)
				end
			else
				Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,0,0), "Denied, you cannot target the group '"..Target.Name.."'" })
			end
		end)
	else
		Ply:SendCustomMessage({ Color(255,255,255), "[", Color(255,20,20), "PERM", Color(255,255,255), "] ", Color(255,0,0), "Denied, you cannot target the group '"..Target.Name.."'" })
	end
end
function GetGroupPermissions(Group, CurrentList, callbackfunction)
	if Group then
		MYSQL:Query("SELECT Inherits, Name FROM "..MYSQL.Tables.Groups.." WHERE ID='"..Group.."';", function(data)
			if data && data[1] then
				MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Permissions.." WHERE GroupID='"..Group.."';", function(deta)
					if deta && deta[1] then
						for _, k in pairs(deta) do
							table.insert(CurrentList, data[1].Name.." -> "..k.Permission)
						end
					end
					GetGroupPermissions(data[1].Inherits, CurrentList, callbackfunction)
				end)
			else callbackfunction(CurrentList) end
		end)
	else callbackfunction(CurrentList) end
end