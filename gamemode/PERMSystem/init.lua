-- /*           Definitions           */ --
PERM = {}
PERM.PLYJOINTIMES = {}
-- /*       Serverside includes       */ --
include("users.lua")
include("groups.lua")
include("cmdhandler.lua")
include("notifications.lua")


-- /*       Clientside includes       */ --
AddCSLuaFile("cl_init.lua")

-- /*         Shared includes         */ --

-- /*            Functions            */ --

util.AddNetworkString("OpenWebURLByPERM")

function PERMPlayerAuthed(Ply, SteamID, UniqueID)
	if not Ply or not Ply:IsValid() then return end
	MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Bans.." WHERE SteamID='"..SteamID.."' AND Expire>=NOW();", function(de)
		if de && de[1] then
			local BanInfo = de[1]
			MYSQL:Query("SELECT Name FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..BanInfo.BannerID.."'", function(name)
				BannerName = ""
				if not name || not name[1] || not name[1].Name then
					BannerName = "Not found"
				else
					BannerName = name[1].Name
				end
				Ply:Kick("=============[BANNED]=============\r\nYou are banned from the server\r\n\r\n[Expires]\r\n"..BanInfo["Expire"].."\r\n\r\n[Banner]\r\n"..BannerName.."("..BanInfo["BannerID"]..")\r\n\r\n[Reason]\r\n"..BanInfo["Reason"])
				Ply:SetOnline(false)
			end)
		else
			MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..SteamID.."'", function(data)
				if not data || not data[1] || data[1][1] == 0 then
					for _, k in pairs(player.GetAll()) do
						k:SendCustomMessage({ Color(255,255,255), "[", Color(255, 20, 20), SERVER_COMMUNITY_NAME, Color(255, 255, 255), "] Please welcome ", Color(80, 255, 0), Ply:Nick(), Color(255, 255, 255), " to the server!"})
					end
					print("["..SERVER_COMMUNITY_NAME.."] New player: "..Ply:Nick().." ["..Ply:SteamID().."]")
					Ply:CreateRecord(SteamID)
				else
					STATS:AddVisits(SteamID, 1);
				end
				
				Ply:LoadGlobalGroup(SteamID)
				Ply:UpdateName()
				PERM:SweepTrashRecords()
				table.insert(PERM.PLYJOINTIMES, { SteamID = Ply:SteamID(), JTime = os.time() })
				MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..SteamID.."' AND Online='1'", function(online)
					if not online || not online[1] || online[1][1] == 0 then
						Ply:SetOnline(true)
						timer.Simple(0.5, function()
							PERM:LogAction(Ply.UserGroup.ID, Ply:SteamID(), "Has connected to the server.")
							if data && data[1] then
								print("[Chat] ["..Ply:GetUserGroup().."] "..Ply:Nick().." joined the server.")
								for _, k in pairs(player.GetAll()) do
									k:SendCustomMessage({ Color(255,255,255), "[", Color(220, 220, 20), SERVER_COMMUNITY_NAME, Color(255,255,255), "] ", Color(20, 255, 20), Ply:Nick(), Color(255,255,255), " has joined the server." })
								end
							end
						end)
					end
				end)
			end)
		end
	end)
end
hook.Add("PlayerAuthed", "PERMPlayerAuthenticated", PERMPlayerAuthed)


function GM:PlayerSay(Ply, msg, ynTeam)
	if Ply && Ply:IsPlayer() then
		Ply:LoadGlobalGroup()
		if string.StartWith(msg, "!") || string.StartWith(msg, "/") then
			if string.len(msg) > 1 then
				local arguments = {}
				for w in string.gmatch(string.sub(msg, 2), "%S+") do table.insert(arguments, w) end
				HandleCommands(Ply, arguments)
			end
		else
			local Message = {}
			local ConsoleLogMSG = ""
			local Targets = {}
			if not Ply:Alive() then
				table.Add(Message, { Color(255,255,255), "[", Color(255,0,0), "Dead", Color(255,255,255), "] " } )
				ConsoleLogMSG = "[DEAD] "
			end
			
			if ynTeam then
				table.Add(Message, { Color(255,255,255), "(", Color(0, 200, 60), "Team", Color(255, 255, 255), ") " } )
				if Ply:Team() == TEAM_HUNTERS then
					ConsoleLogMSG = ConsoleLogMSG.."[Hunters] " 
				elseif Ply:Team() == TEAM_PROPS then
					ConsoleLogMSG = ConsoleLogMSG.."[Props] " 
				else
					ConsoleLogMSG = ConsoleLogMSG.."[SPECTATOR] " 
				end
				Targets = team.GetPlayers(Ply:Team())
			else
				Targets = player.GetAll()
			end
			
			table.Add(Message, { Color(255,255,255), "[", Color(Ply.UserGroup["Red"], Ply.UserGroup["Green"], Ply.UserGroup["Blue"]), Ply:GetUserGroup(), Color(255,255,255), "] "..Ply:Nick()..": "..msg })
			for _, k in pairs(Targets) do
				k:SendCustomMessage(Message)
			end
			print("(Chat) "..ConsoleLogMSG.."["..Ply:GetUserGroup().."] "..Ply:Nick().. ": "..msg)
		end
	end
	return ""
end
function GM:PlayerDisconnected( Ply )

end
gameevent.Listen( "player_disconnect" )
hook.Add("player_disconnect", "PERMPlayerDisconnect", function(Data)
	local Ply = player.GetBySteamID(Data.networkid)
	if Ply && Ply:IsPlayer() && Ply:IsValid() && !Ply:IsBot() then
		Ply:SetOnline(false)
		PERM:LogAction(Ply.UserGroup.ID, Ply:SteamID(), "Has disconnected from the server.")
		for _,k in pairs(PERM.PLYJOINTIMES) do
			if k.SteamID == Ply:SteamID() then
				MYSQL:Query("SELECT 1 FROM "..MYSQL.Tables.PlayTimes.." WHERE SteamID='"..Ply:SteamID().."';", function(data)
					if not data || not data[1] || data[1][1] == 0 then
						MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.PlayTimes.." (SteamID, Time) VALUES ('"..k["SteamID"].."', "..(os.time() - tonumber(k["JTime"]))..");")
					else
						MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.PlayTimes.." SET Time = Time + "..(os.time() - tonumber(k.JTime)).." WHERE SteamID='"..k["SteamID"].."';")
					end
					table.remove(PERM.PLYJOINTIMES, _)
				end)
			end
		end
		print("[Chat] ["..Ply:GetUserGroup().."] "..Ply:Nick().." disconnected, reason: "..Data.reason)
		for _, k in pairs(player.GetAll()) do
			k:SendCustomMessage({ Color(255,255,255), "[", Color(220, 220, 20), SERVER_COMMUNITY_NAME, Color(255,255,255), "] ", Color(Ply.UserGroup["Red"], Ply.UserGroup["Green"], Ply.UserGroup["Blue"]), Ply:Nick(), Color(255, 20, 20)," has disconnected, reason: "..Data.reason })
		end
	end
end)

function PERM:LogAction(GroupID, SteamID, Message)
	Message = MYSQL.DB:escape(Message)
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Log.." (ServerID, GroupID, SteamID, Message, WebUI) VALUES ('"..MYSQL.InstanceID.."', '"..GroupID.."', '"..SteamID.."', '"..Message.."', 0)")
end


function PERMInitialize()
	timer.Create("PERMSaveActivTimesTimer", 5, 0, PERMSaveActiveTimes)
	MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Online=0 WHERE LastSeenOn='"..MYSQL.InstanceID.."'");
end
hook.Add("Initialize", "PERMInitialize", PERMInitialize)
concommand.Add("")

function PERMShutdown()
	PERMSaveActiveTimes()
	timer.Stop("PERMSaveActivTimesTimer")
	PERM.PLYJOINTIMES = nil
end
hook.Add("ShutDown", "PERMShutdown", PERMShutdown)

function PERMSaveActiveTimes()
	for _,k in pairs(PERM.PLYJOINTIMES) do
		MYSQL:Query("SELECT * FROM "..MYSQL.Tables.PlayTimes.." WHERE SteamID='"..k.SteamID.."';", function (data)
			if not data || not data[1] then
				MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.PlayTimes.." (SteamID, Time) VALUES ('"..k.SteamID.."', "..(os.time() - tonumber(k.JTime))..");")
			else
				MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.PlayTimes.." SET Time = Time + "..(os.time() - tonumber(k.JTime)) .." WHERE SteamID='"..k.SteamID.."';")
			end
			k.JTime = os.time()
		end)
	end
end