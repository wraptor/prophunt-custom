local ReceivedItems = {}
local Advertisements = {
	{ Color(255, 0, 200), "You can view all your stats on our website ", Color(0,255,0), "https://prophunt.vlrpgaming.com/Players/" },
	{ Color(0,177,255), "Have you read the rules? Our website contains all information like rules, updates, planned maintenances and much more. ", Color(0,255,0), "https://prophunt.vlrpgaming.com" },
	{ Color(255,0,0), "Follow the rules! Your reputation depends on it. Banned on this server means banned on all Prophunt servers." },
	{ Color(255,0,0), "Warnings can become in-active over time. This will only happen if you've got a good behavior." },
	{ Color(0, 177, 255), "You can donate to our server, we will reward you for doing so. Visit ", Color(0, 255, 0), "https://prophunt.vlrpgaming.com/Donate/" },
	{ Color(0, 177, 255), "You can buy special abilities to give yourself an advantage! Press ", Color(0, 255, 0), "F4", Color(0, 177,255), "!" },
	{ Color(0, 177, 255), "You can taunt randomly by pressing ", Color(0, 255, 0), "F3", Color(0, 177,255), " or if you prefer to taunt a specific sound press ", Color(0, 255, 0), "C", Color(0, 177,255), " and chose the taunt you like." }
}

function NotificationsSyncersInitialize()
	CheckTimers()
end
hook.Add("Initialize", "NotificationsSyncersInitialize", NotificationsSyncersInitialize)

function CreateSyncNotification( ... )
	CheckTimers()
	
	local tble = {}
	for _,k in pairs( {...} ) do
		if type(k) == "string" then k = MYSQL.DB:escape(k) end
		table.insert(tble, k)
	end
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Notifications.." (Expire, Message, Type) VALUES (DATE_ADD(NOW(), INTERVAL 1 SECOND), '"..string.Replace(util.TableToJSON( tble ), "\\'", "\'").."', 0)")
end
function CreateSyncListenup(...)
	CheckTimers()
	
	local tble = {}
	for _,k in pairs( {...} ) do
		if type(k) == "string" then k = MYSQL.DB:escape(k) end
		table.insert(tble, k)
	end
	
	MYSQL:ExecuteNonQuery("INSERT INTO "..MYSQL.Tables.Notifications.." (Expire, Message, Type) VALUES (DATE_ADD(NOW(), INTERVAL 1 SECOND), '"..string.Replace(util.TableToJSON( tble ), "\\'", "\'").."', 1)")
end
function CheckTimers()
	if not timer.Exists("SyncMyShitSystem") then
		timer.Create("SyncMyShitSystem", 1,0, CheckForNotification)
	end
	if not timer.Exists("AdvertisingMyShitSystem") then
		timer.Create("AdvertisingMyShitSystem", 300,0, PostAdvertisement)
	end
end


function CheckForNotification()
	CheckTimers()
	
	MYSQL:ExecuteNonQuery("DELETE FROM "..MYSQL.Tables.Notifications.." WHERE Expire < NOW();")
	MYSQL:Query("SELECT * FROM "..MYSQL.Tables.Notifications, function(entries)
		if entries then
			for _,k in pairs(entries) do
				if not table.HasValue(ReceivedItems, k["ID"]) then
					local Message = util.JSONToTable( k["Message"] )
					table.insert(ReceivedItems, k["ID"])
					timer.Simple(2, function() table.RemoveByValue(ReceivedItems, k["ID"]) end)
					if k["Type"] == 0 then
						local Prefix = { Color(255,255,255), "[", Color(255,50,0), SERVER_COMMUNITY_NAME, Color(255,255,255), "] " }
						for _, r in pairs(Message) do
							table.insert(Prefix, r)
						end
						for _, Ply in pairs(player.GetAll()) do
							Ply:SendCustomMessage(Prefix)
						end
					elseif k["Type"] == 1 then
						local strMessage = ""
						for _, r in pairs(Message) do
							if type(r) == "string" then strMessage = strMessage.." "..r end
						end
						for _, Ply in pairs(player.GetAll()) do
							Ply:SendListenup(strMessage)
						end
						print("\r\n[Listenup] "..strMessage.."\r\n")
					end
				end
			end
		end
	end)
end

function PostAdvertisement()
	local Ad = Advertisements[ math.random( #Advertisements ) ]
	for _, Ply in pairs(player.GetAll()) do
		if Ply:IsValid() then
			local Message = { Color(255,255,255), "[", Color(255,150, 0), SERVER_COMMUNITY_NAME, Color(255,255,255), "] " }
			table.Add(Message, Ad)
			Ply:SendCustomMessage(Message)
		end
	end
end