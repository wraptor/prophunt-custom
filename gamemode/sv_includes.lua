if SERVER then
	-- MySQL System (IMPORTANT)
	include("MySQL/init.lua")
	
	-- Permissions System
	include("PERMSystem/init.lua")
	
	-- Gamemode based
	include("sh_init.lua")
	include("sv_admin.lua")

	-- Pointshop system
	include("pointshop/sv_init.lua")

	-- Stats system
	include("statssystem/sv_init.lua")

	-- Taunging system
	include("taunting/sv_init.lua")
end