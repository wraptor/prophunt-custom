-- Serverside only POINTS_ITEMS information

-- /*        HUNTERS        */
function POINTS_ITEMS.TAUNT_FORCE:Handler(Pl)
	if team.NumPlayers(TEAM_PROPS) > 0 then
	
		local Ply = table.Random(team.GetPlayers(TEAM_PROPS))
		while not Ply:Alive() do
			Ply = table.Random(team.GetPlayers(TEAM_PROPS))
		end
		local Taunt = table.Random(PROP_TAUNTS)
		local duration = SoundDuration(Taunt)
		Ply:EmitSound(Taunt, 100)
		if duration > 0 && duration < 10 then
			Ply.last_taunt_duration = duration
		else
			Ply.last_taunt_duration = 2
		end
	else
		Pl:SendLua( [[notification.AddLegacy("What a shame, the props team is empty. We've refunded you.", NOTIFY_GENERIC, 3 )]] )
		Pl:AddCurrentPoints(self.Price)
	end
end
function POINTS_ITEMS.TAUNT_EVERYBODY:Handler(Ply)
	local Players = {}
	for _, k in pairs(team.GetPlayers(TEAM_PROPS)) do
		if k:Alive() then
			table.insert(Players, k)
		end
	end
	if #Players > 0 then
		TauntPlayers(1, (#Players), Players)
	else
		Ply:SendLua( [[notification.AddLegacy("What a shame, the props team is empty or has nobody alive. We've refunded you.", NOTIFY_GENERIC, 3 )]] )
		Ply:AddCurrentPoints(self.Price)
	end
end
function POINTS_ITEMS.HIDING_PROP_NAME:Handler(Ply)
	if team.NumPlayers(TEAM_PROPS) > 0 then
		local Prop = table.Random(team.GetPlayers(TEAM_PROPS))
		Ply:SendLua([[ notification.AddLegacy("Someone is a: ]]..Prop.ph_prop:GetName()..[[", NOTIFY_GENERIC, 90 ) ]])
		Prop:SendLua([[ notification.AddLegacy("]]..Ply:Nick()..[[ knows what you are!", NOTIFY_GENERIC, 90 ) ]])
	else
		Ply:SendLua( [[notification.AddLegacy("What a shame, the props team is empty. We've refunded you.", NOTIFY_GENERIC, 5 )]] )
		Ply:AddCurrentPoints(self.Price)
	end
end
function POINTS_ITEMS.HIDING_PROP_LIST:Handler(Ply)
	if team.NumPlayers(TEAM_PROPS) > 0 then
		local Prop = nil
		repeat
			Prop = table.Random(team.GetPlayers(TEAM_PROPS))
		until Prop.ph_prop ~= nil
		local int = math.random(0, 5)
		local strng = "List of models:\r"
		
		for i = 0,4 do
			if i == int then
				strng = strng .. Prop.ph_prop:GetName() .. "\r"
			else
				local entity = Ply
				repeat
					ent = ents.GetAll()
					entity = ent[math.random(#ent)]
				until (not entity:IsPlayer() || entity:GetModel() == Prop.ph_prop:GetModel()) && entity:GetModel()
				
				strng = strng .. entity:GetModel() .. "\r"
			end
		end
		Ply:ChatPrint(strng)
	else
		Ply:SendLua( [[notification.AddLegacy("What a shame, the props team is empty. We've refunded you.", NOTIFY_GENERIC, 5 )]] )
		Ply:AddCurrentPoints(self.Price)
	end
end
function POINTS_ITEMS.BUY_GRENADE:Handler(Ply)
	Ply:GiveAmmo(1, "SMG1_Grenade")
end
-- /*         PROPS         */
function POINTS_ITEMS.FREEZE_CLOSEST_HUNTER:Handler(Ply)
	local nearestply;
	local range = 15000000
    for i, pl in ipairs( player.GetAll() ) do
		print(pl:Nick())
		if pl:Team() == TEAM_HUNTERS && pl:Alive() then
			local distance = Ply:GetPos():Distance( pl:GetPos() )
			if( distance <= range) then
				nearestply = pl
				range = distance
			end
		end
    end
	if nearestply == nil then
		Ply:SendLua( [[notification.AddLegacy("There aren't any hunters in the area, you've been refunded.", NOTIFY_HINT, 5 )]] )
		Ply:AddCurrentPoints(self.Price) return
	elseif (CurTime() - GetGlobalFloat( "RoundStartTime" )) > HUNTER_BLINDLOCK_TIME then
		nearestply:Freeze(true)
		timer.Simple(5, function() nearestply:Freeze(false) end)
	else
		Ply:SendLua( [[notification.AddLegacy("The hunters are still blinded, wait a bit.", NOTIFY_HINT, 5 )]] )
		Ply:AddCurrentPoints(self.Price)
	end
end
function POINTS_ITEMS.FREEZE_ALL_HUNTERS:Handler(Ply)
	local FrozenPlayers = {}
	for i, pl in pairs( team.GetPlayers(TEAM_HUNTERS) ) do
		if pl:Alive() then
			pl:SendLua([[notification.AddLegacy("]]..Ply:Nick()..[[ froze you for 10 seconds.", NOTIFY_HINT, 5 )]])
			pl:Freeze(true)
			table.insert(FrozenPlayers, pl)
		end
	end
	timer.Simple(10, function()
		for i, pl in pairs(FrozenPlayers) do
			pl:Freeze(false)
		end
	end)
end
function POINTS_ITEMS.BECOME_INVISIBLE:Handler(pl)
	if pl.ph_prop == nil then return end
	local ent = ents.Create("ph_prop")
	ent:SetModel(pl.ph_prop:GetModel())
	ent:SetName(pl.ph_prop:GetModel())
	ent:SetSkin(pl.ph_prop:GetSkin())
	ent:SetSolid(pl.ph_prop:GetSolid())
	ent:SetAngles(pl.ph_prop:GetAngles())
	ent:SetOwner(pl)
	if !GetConVar("ph_better_prop_movement"):GetBool() then
		ent:SetParent(pl)
	end
	ent.max_health = pl.ph_prop.max_health
	ent.health = pl.ph_prop.health
	
	pl:SetNWEntity("PlayerPropEntity", ent)
	
	pl.ph_prop:Remove()
	pl:RemoveClientProp()
	
	pl:SendLua([[notification.AddLegacy("You are now invisible.", NOTIFY_HINT, 10 )]])
	timer.Simple(10, function()
		pl.ph_prop = ent
		pl.ph_prop:Spawn()
		
		--pl:RespawnClientProp()
		pl:SendLua([[notification.AddLegacy("You've regained visibility.", NOTIFY_HINT, 3 )]])
	end)
end
function POINTS_ITEMS.FAKE_TAUNT:Handler(Ply)
	local nEntity = table.Random(ents.GetAll())
    while nEntity:IsPlayer() || nEntity:GetParent():IsPlayer() || nEntity:GetOwner():IsPlayer() do
		nEntity = table.Random(ents.GetAll())
	end
	nEntity:EmitSound(table.Random(PROP_TAUNTS), 100)
end
function POINTS_ITEMS.BECOME_REAL:Handler(Ply)
	--Ply.ph_prop.real = true
	--Ply:Freeze(true)
end
function TauntPlayers(Int, MaxInt, Props)
	local Taunt = table.Random(PROP_TAUNTS)
	local Duration = SoundDuration(Taunt)
	Props[Int]:EmitSound(Taunt, 100)
	if Int < MaxInt then
		timer.Simple(Duration + 1, function() TauntPlayers((Int+1), MaxInt, Props) end)
	end
end