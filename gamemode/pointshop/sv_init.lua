include("sv_database.lua")
include("sh_items.lua")
include("sv_items_handlers.lua")

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("sh_items.lua")
AddCSLuaFile("cl_transactions.lua")

-- Pointshop toggle network string
util.AddNetworkString("OpenPointShop")
util.AddNetworkString("BuyTransaction")
util.AddNetworkString("UpdateMyPoints")
util.AddNetworkString("ReceiveMyPoints")
util.AddNetworkString("UseBoughtItem")

function GM:ShowSpare2(ply)
	SendPlayerPoints(ply)
	if ply:Team() == TEAM_HUNTERS || ply:Team() == TEAM_PROPS then
		net.Start("OpenPointShop")
		net.Send(ply)
	end
end

function AllowedToBuy(ITEM, Ply )
	local Allowed = false
	if ITEM["GroupsOnly"] == true then
		for k,v in pairs(ITEM["Groups"]) do
			if Ply:IsUserGroup(v) then Allowed = true end
		end
	else Allowed = true end
	return Allowed
end

net.Receive("BuyTransaction", function(len, Ply)
	local str = net.ReadString()
	local ITEM = nil
	for k,v in pairs(POINTS_ITEMS) do
		if k == str then
			ITEM = v
		end
	end
	
	local EnemeyTeam = 0
	if Ply:Team() == TEAM_HUNTERS then
		EnemeyTeam = TEAM_PROPS
	else
		EnemeyTeam = TEAM_HUNTERS
	end
	
	if ITEM == nil then
		CompleteTransaction("The item you tried to buy doesn't exist.", 1, Ply) return
	elseif Ply:Team() ~= tonumber(ITEM["Team"]) then
		CompleteTransaction("You must be on the other team to use this item.", 1, Ply) return
	elseif not AllowedToBuy(ITEM, Ply) then
		CompleteTransaction("You don't have the right rank to buy this item. ["..ITEM["Groups"][#ITEM["Groups"]].." required.]", 1, Ply) return
	elseif not Ply:Alive() || Ply:Team() == TEAM_SPECTATOR then 
		CompleteTransaction("You must be alive in order to buy items.", 1, Ply) return
	elseif GetGlobalBool("InRound") == false then  
		CompleteTransaction("You must be in a round before you can buy items.", 1, Ply) return
	elseif #team.GetPlayers(EnemeyTeam) == 0 then
		CompleteTransaction("The enemy team is empty.", 1, Ply) return
	elseif Ply:HasCooldown(str) then
		CompleteTransaction("This item is still under cooldown.", 2, Ply) return
	else
		MYSQL:Query("SELECT Points FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..Ply:SteamID().."';", function(data)
			if data && data[1] then
				if tonumber(data[1].Points) > ITEM.Price then
					Ply:AddCooldown(str, ITEM.Cooldown)
					Ply:RemoveCurrentPoints(ITEM.Price)
					Ply.BoughtPointsItem = ITEM
					CompleteTransaction("You've successfully bought the item. Press H to use.", 0, Ply)
					STATS:AddBoughtItem(Ply, ITEM.Name)
					//ITEM:Handler(Ply) -- Do not execute (You can activate it whenever you like to.)
				else
					CompleteTransaction("You do not have enough money to buy this item.", 1, Ply) return					
				end
			end
		end)
	end
end)

function POINTSPlayerSpawn(Ply)
	if Ply.BoughtPointsItem then
		Ply.BoughtPointsItem = nil
		CompleteTransaction("You didn't use your item, it's been removed.", 1, Ply)
	end
end
hook.Add("PlayerSpawn", "POINTSPlayerSpawn", POINTSPlayerSpawn)


function CompleteTransaction( Answer, notifytype, Ply )
	Ply:SendLua([[ notification.AddLegacy("]]..Answer..[[", ]]..notifytype..[[, 3 ) ]])
end
function SendPlayerPoints(Ply)
	MYSQL:Query("SELECT Points FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..Ply:SteamID().."';", function(data)
		if data && data[1] then
			net.Start("ReceiveMyPoints")
			net.WriteInt(data[1].Points, 32)
			net.Send(Ply)
			
			Ply:SetNWInt("MyPoints", data[1].Points)
		end
	end)
	
end
net.Receive("UpdateMyPoints", function(len, Ply)
	SendPlayerPoints(Ply)
end)

net.Receive("UseBoughtItem", function(len, Ply)
	if Ply.BoughtPointsItem then
		Ply.BoughtPointsItem:Handler(Ply)
		CompleteTransaction(Ply.BoughtPointsItem.Name.." has been activated.", 0, Ply)
		Ply.BoughtPointsItem = nil
	end
end)