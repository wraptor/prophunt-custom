POINTS_ITEMS = {
	-- Hunters
	TAUNT_FORCE = {
		Name = "Force taunt", -- Name for item.
		Description =         -- Description for the item.
			"Force a random prop to taunt, also there's a chance a random non player prop will taunt instead.",
		Team = 1,             -- What team can buy this item? 1 for Hunters, 2 for Props
		Price = 1000,          -- Price in coins.
		Cooldown = 5,         -- Cooldown in minutes... MINUTES.
		GroupsOnly = false    -- Specify if only specific groups are allowed to use this item.
	},
	TAUNT_EVERYBODY = {
		Name = "Force taunt for everybody",
		Description = "This forces all props to taunt for you, one by one so be fast or they'll be done taunting. Keep in mind: This is really expensive.",
		Team = 1,
		Price = 5000,
		Cooldown = 60,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Administrator",
			"Moderator",
			"MVP+",
			"MVP"
		}
	},
	HIDING_PROP_NAME = {
		Name = "Ghost prop name",
		Description = "This will tell you the prop name a player is hiding as.",
		Team = 1,
		Price = 5000,
		Cooldown = 15,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Administrator",
			"Moderator",
			"MVP+",
			"MVP",
			"VIP+"
		}
	},
	HIDING_PROP_LIST = {
		Name = "Ghost prop names",
		Description = "This will tell you a real hider's prop name, the others are fake. You won't be told which one though!",
		Team = 1,
		Price = 1000,
		Cooldown = 60,
		GroupsOnly = false
	},
	BUY_GRENADE = {
		Name = "SMG Grenade",
		Description =
			"This will give you a SMG Grenade to bomb all those crazy unhittable props.",
		Team = 1,
		Price = 1000,
		Cooldown = 15,
		GroupsOnly = false
	},
	-- Props
	FREEZE_CLOSEST_HUNTER = {
		Name = "Freeze nearby hunter",
		Description =
			"Freezes the closest hunter for 5 seconds so you can make your big escape.",
		Team = 2,
		Price = 1000,
		Cooldown = 15,
		GroupsOnly = false
	},
	FREEZE_ALL_HUNTERS = {
		Name = "Freeze all hunters",
		Description =
			"Freezes all hunters for 10 seconds so you can flawlessly escape.",
		Team = 2,
		Price = 5000,
		Cooldown = 20,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Administrator",
			"Moderator",
			"MVP+",
			"MVP"
		}
	},
	BECOME_INVISIBLE = {
		Name = "Become invisible",
		Description = 
			"This will make you invisible for 10 seconds allowing you to sneek around a hunter. Or try and make them crazy.",
		Team = 0,
		Price = 3000,
		Cooldown = 15,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Administrator",
			"Moderator",
			"MVP+"
		}
	},
	FAKE_TAUNT = {
		Name = "Fake Taunt",
		Description =
			"Make another entity (non player entity) taunt distracting all hunters.",
		Team = 2,
		Price = 200,
		Cooldown = 10,
		GroupsOnly = false
	},
	BECOME_REAL = {
		Name = "Fake as real prop",
		Description =
			"This will make your prop handle like a real prop, you will fall down, you will move when pushed. You can't move by yourself though.",
		Team = 0,
		Price = 10000,
		Cooldown = 60,
		GroupsOnly = true,
		Groups = {
			"Owner",
			"Co-Owner",
			"Administrator",
			"Moderator",
			"MVP+"
		}
	}
}