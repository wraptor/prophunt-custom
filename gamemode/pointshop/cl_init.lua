include("sh_items.lua")
include("cl_transactions.lua")

local Ply = LocalPlayer()
local PANEL = FindMetaTable("Panel")

MyPoints = 0

surface.CreateFont( "POINTS_FONT_BIG", {
	font = "Arial", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 46,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )
surface.CreateFont( "POINTS_FONT_MEDIUM", {
	font = "Arial", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 24,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
} )

function OpenPointShop()
	net.Start("UpdateMyPoints")
	net.SendToServer()
	timer.Simple(0.1, function()
		local Pointshop = vgui.Create("DFrame")
		Pointshop:SetTitle("")
		Pointshop:SetVisible(true)
		Pointshop:SetSize(800, 600)
		Pointshop:Center()
		Pointshop:ShowCloseButton(true)
		Pointshop:MakePopup()
		Pointshop.Paint = function()
			draw.RoundedBox(4,0,0, Pointshop:GetWide(), Pointshop:GetTall(), Color(50,50,50))
		end
		local lblPoints = vgui.Create("DLabel", Pointshop)
		lblPoints:SetText("Points: "..MyPoints)
		lblPoints:SetFont("POINTS_FONT_BIG")
		lblPoints:SetSize(784, 46)
		lblPoints:SetPos(15, 7)
		lblPoints:SetBright(1)
		
		local lblName = vgui.Create("DLabel", Pointshop)
		lblName:SetText(LocalPlayer():Nick())
		lblName:SetFont("POINTS_FONT_MEDIUM")
		lblName:SetSize(784, 24)
		lblName:SetPos(15, 53)
		lblName:SetBright(1)
		
		
		local pnlScrollList = vgui.Create("DScrollPanel", Pointshop)
		pnlScrollList:SetSize(800, 512)
		pnlScrollList:SetPos(0, 88)
		
		
		local pnlItems = vgui.Create("DIconLayout", pnlScrollList)
		pnlItems:SetSize(770, 512)
		pnlItems:SetPos(15,0)
		
		for k, v in pairs(POINTS_ITEMS) do
			if v["Team"] == LocalPlayer():Team() then
				local Allowed = false
				if v["GroupsOnly"] == true then
					for _,k in pairs(v["Groups"]) do
						if LocalPlayer():IsUserGroup(k) then Allowed = true end
					end
				else
					Allowed = true
				end
				if Allowed then
					local res = nil
					net.Start("DoIHaveCooldown")
					net.WriteString(k)
					net.SendToServer()
					net.Receive("ResultDoIHaveCooldown", function()
						local res = net.ReadString()
						local v = POINTS_ITEMS[res]
						if not net.ReadBool() then
							local ListItem = pnlItems:Add("DPanel")
							ListItem:SetSize(783, 64)
							ListItem:SetBackgroundColor(team.GetColor(v["Team"]))
							ListItem:SetCursor("hand")
							ListItem.PointsItem = res
							function ListItem:OnCursorEntered()
								function ListItem.Paint()
									local col = ListItem:GetBackgroundColor()
									col.a = 200
									surface.SetDrawColor(col)
									surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
								end
							end
							function ListItem:OnCursorExited()
								function ListItem.Paint()
									local col = ListItem:GetBackgroundColor()
									col.a = 255
									surface.SetDrawColor(col)
									surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
								end
							end
							function ListItem:OnMousePressed( key )
								if key == MOUSE_LEFT then
									PlaySelectedSound(self.Taunt)
									TauntMenu:Close()
								end
							end
							function ListItem.Paint()
								surface.SetDrawColor(team.GetColor(LocalPlayer():Team()))
								surface.DrawRect( 0, 0, ListItem:GetWide(), ListItem:GetTall() )
							end
							function ListItem:OnMousePressed( key )
								if key == MOUSE_LEFT then
									BuyPointsItem(self.PointsItem)
									Pointshop:Close()
								end
							end
							
							local lblName = vgui.Create("DLabel", ListItem)
							lblName:SetText(v["Name"])
							lblName:SetFont("POINTS_FONT_MEDIUM")
							lblName:SetSize(655, 32)
							lblName:SetPos(4,4)
							lblName:SetBright(1)
							
							local lblPrice = vgui.Create("DLabel", ListItem)
							lblPrice:SetPos(663, 4)
							lblPrice:SetSize(112, 28)
							lblPrice:SetText(v["Price"].." Points")
							lblPrice:SetBright(1)
							
							local lblDescription = vgui.Create("DLabel", ListItem)
							lblDescription:SetPos(4, 32)
							lblDescription:SetSize(783, 28)
							lblDescription:SetText(v["Description"])
							lblDescription:SetBright(1)
						end
					end)
				end
			end
		end
	end)
end 

function UpdatePoints()
	net.Start("UpdateMyPoints")
	net.SendToServer()
	return Ply:GetNWInt("MyPoints")
end

hook.Add("Think", "ThinkUseItemThing", function()
	if LocalPlayer():Alive() && not LocalPlayer():IsTyping() && (LocalPlayer():Team() == TEAM_HUNTERS || LocalPlayer():Team() == TEAM_PROPS) && input.IsKeyDown(KEY_H) then
		net.Start("UseBoughtItem")
		net.SendToServer()
	end
end)

net.Receive("OpenPointShop", OpenPointShop)
net.Receive("ReceiveMyPoints", function()
	MyPoints = net.ReadInt(32)
end)