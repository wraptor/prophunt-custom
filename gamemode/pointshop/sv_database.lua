local PLAYER = FindMetaTable("Player")

util.AddNetworkString("ResultDoIHaveCooldown")
util.AddNetworkString("DoIHaveCooldown")

function PLAYER:GetCurrentPoints()
	self:SetNWInt("MyPoints", value[1].Points)
end
function PLAYER:SetCurrentPoints( pnts )
	if type(pnts) == "number" then
		MYSQL:ExecuteNonQuery("UPDATE "..MYSQL.Tables.Players.." SET Points = ".. pnts .." WHERE SteamID='"..self:SteamID().."';")
		self:SetNWInt("MyPoints", pnts)
	end
end
function PLAYER:AddCurrentPoints( pnts )
	MYSQL:Query("SELECT Points FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..self:SteamID().."';", function(data)
		if data && data[1] then
			self:SetCurrentPoints(data[1].Points + pnts)
		end
	end)
end
function PLAYER:RemoveCurrentPoints ( pnts )
	MYSQL:Query("SELECT Points FROM "..MYSQL.Tables.Players.." WHERE SteamID='"..self:SteamID().."';", function(data)
		if data && data[1] then
			self:SetCurrentPoints(data[1].Points - pnts)
		end
	end)
end

function PLAYER:AddPlayingReward()
	self:AddCurrentPoints(10)
end

function PLAYER:HasCooldown(ITEM)
	if self.Cooldowns == nil then
		self.Cooldowns = {}
		return false
	end
	for k,v in pairs(self.Cooldowns) do
		if v == ITEM then
			return true
		end
	end
end
function PLAYER:AddCooldown(ITEM, Time)
	if self.Cooldowns == nil then
		self.Cooldowns = {}
	end
	table.insert(self.Cooldowns, ITEM)
	timer.Simple((Time*60), function() RemoveCooldown(ITEM, self) end)
end
function RemoveCooldown(ITEM, Ply)
	if Ply.Cooldowns == nil then return end
	table.RemoveByValue(Ply.Cooldowns, ITEM)
end

net.Receive("DoIHaveCooldown", function(len, Ply)
	local val = net.ReadString()
	net.Start("ResultDoIHaveCooldown")
	net.WriteString(val)
	net.WriteBool(Ply:HasCooldown(val))
	net.Send(Ply)
end)