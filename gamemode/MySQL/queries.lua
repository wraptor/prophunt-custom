function MYSQL.DB:onConnectionFailed( err )
	print(STATS.PREFIX.."DB connection failed, Error:\r"..err)
end
function MYSQL:ExecuteNonQuery( query )
	local Query = MYSQL.DB:query(query)
	function Query:onError(err, q)
		print("\r\n\r\n[WARNING]\r\nQuery error ["..q.."]: \r\n\r\n"..err)
		Verify()
	end
	Query:start()
end
function MYSQL:Exists( query )
	local Query = MYSQL.DB:query( query )
	result = nil
	function Query:onSuccess( data )
		local res = table.Random( data[1] )
		result = (res ~= nil && res)
	end
	function Query:onError(err, q)
		print("\r\n\r\n[WARNING]\r\nQuery error ["..q.."]: \r\n\r\n"..err)
		Verify()
	end
	Query:start()
	Query:wait()
	return result
end
function MYSQL:ExecuteScalar( query )
	local result = nil
	local Query = MYSQL.DB:query(query)
	function Query:onSuccess( data )
		result = data
	end
	function Query:onError(err, q)
		print("\r\n\r\n[WARNING]\r\nQuery error ["..q.."]: \r\n\r\n"..err)
		Verify()
	end
	Query:start()
	Query:wait()
	return result
end

function Verify()
	if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
		MYSQL.DB:connect()
		return
	end
end