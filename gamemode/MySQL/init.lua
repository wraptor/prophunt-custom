require("mysqloo")
MYSQL = {
	Server = "160.153.161.239",
	Database = "vlrpgaming_prophunt",
	Username = "vlrpwraptor",
	Password = "hmwywYna1KjvN@n!MfaMc56Bas@DMuSQbb(z5nZ)Ya#Hv",
	Queue = {},
	-- Tables.
	Tables = {
		Players = "players",
		Bans = "bans",
		Groups = "groups",
		Permissions = "permissions",
		Warns = "warns",
		Kicks = "kicks",
		Kills = "log_kills",
		Log = "log",
		PlayTimes = "playtimes",
		TauntCounts = "counts_taunts",
		PointsHistory = "shop_history",
		Notifications = "notifications"
	},
	InstanceID = 1
}
MYSQL.DB = mysqloo.connect( MYSQL.Server, MYSQL.Username, MYSQL.Password, MYSQL.Database, 3306)

-- Handling
function MYSQL.DB:onConnected()
	for k, v in pairs( MYSQL.Queue ) do MYSQL:Query( v[ 1 ], v[ 2 ] ) end
	MYSQL.Queue = {}
	local query_name = self:query("SET NAMES utf8");
	query_name:start();
end

MYSQL.DB:connect()

function MYSQL:Query(query, callback)
	local Query = MYSQL.DB:query(query)
	if Query then
		function Query:onSuccess(data)
			callback(data)
		end
		function Query:onError( err )
			if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
				table.insert( MYSQL.Queue, { query, callback } )
				MYSQL.DB:connect()
			end
			print("\r\n\r\n[WARNING]\r\n\r\n"..err.."\r\n\r\nOn Query: \r\n"..query)
		end
		Query:start()
	else
		if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
			table.insert( MYSQL.Queue, { query, callback } )
			MYSQL.DB:connect()
		end
	end
end


function MYSQL:ExecuteNonQuery( query )
	local Query = MYSQL.DB:query(query)
	if Query then
		function Query:onError( err )
			if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
				table.insert( MYSQL.Queue, { query, callback } )
				MYSQL.DB:connect()
			end
			print("\r\n\r\n[WARNING]\r\n\r\n"..err.."\r\n\r\nOn Query: \r\n"..query)
		end
		Query:start()
	else
		if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
			table.insert( MYSQL.Queue, { query, callback } )
			MYSQL.DB:connect()
		end
	end
end
function MYSQL:Exists( query )
	local Query = MYSQL.DB:query( query )
	result = nil
	function Query:onSuccess( data )
		local res = table.Random( data[1] )
		result = (res ~= nil && res)
	end
	function Query:onError(err, q)
		print("\r\n\r\n[WARNING]\r\n\r\n"..err)
		Verify()
	end
	Query:start()
	Query:wait()
	return result
end
function MYSQL:ExecuteScalar( query )
	local result = nil
	local Query = MYSQL.DB:query(query)
	function Query:onSuccess( data )
		result = data
	end
	function Query:onError(err, q)
		print("\r\n\r\n[WARNING]\r\n\r\n"..err)
		Verify()
	end
	Query:start()
	Query:wait()
	return result
end

function Verify()
	if MYSQL.DB:status() == mysqloo.DATABASE_NOT_CONNECTED then
		MYSQL.DB:connect()
		return
	end
end