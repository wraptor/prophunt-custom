"Gamemode"
{
	"base"		"base"
	"title"		"Prop Hunt"
	"maps"      "^ph_"

	"fretta_maps"
        {
    "1"	  "ph_"
        }
        "selectable"            "1"
        
	"menusystem"	"1"
	"workshopid"	"417565863"
	"settings"
	{
		1
		{
			"name"		"ph_hunter_fire_penalty"
			"text"		"Hunter Fire Penality"
			"help"		"Health points removed from hunters when they shoot"
			"type"		"Numeric"
			"default"	"5"
		}

		2
		{
			"name"		"ph_hunter_kill_bonus"
			"text"		"Hunter Kill Bonus"
			"help"		"How much health to give back to the Hunter after killing a prop"
			"type"		"Numeric"
			"default"	"100"
		}

		3
		{
			"name"		"ph_swap_teams_every_round"
			"text"		"Swap Teams Every Round"
			"help"		"Should teams swapped on every round?"
			"type"		"CheckBox"
			"default"	"1"
		}

		4
		{
			"name"		"ph_game_time"
			"text"		"Time Left (Mins.)"
			"help"		"Maxium Time Left (in minutes) - Default is 30 minutes."
			"type"		"Numeric"
			"default"	"30"
		}
		
		5
		{
			"name"		"ph_hunter_blindlock_time"
			"text"		"Hunter Blind Time (Sec.)"
			"help"		"How long hunters are blinded (in seconds)"
			"type"		"Numeric"
			"default"	"30"
		}
		
		6
		{
			"name"		"ph_round_time"
			"text"		"Round Time (Sec.)"
			"help"		"Time (in seconds) for each rounds."
			"type"		"Numeric"
			"default"	"300"
		}
		
		7
		{
			"name"		"ph_rounds_per_map"
			"text"		"Rounds per Map (Num.)"
			"help"		"Numbers played on a map (Default: 10)"
			"type"		"Numeric"
			"default"	"10"
		}
		
		8
		{
			"name"		"ph_prop_camera_collisions"
			"text"		"Prop View Collisions"
			"help"		"Props can/cannot see through walls."
			"type"		"CheckBox"
			"default"	"1"
		}
		
		9
		{
			"name"		"ph_freezecam"
			"text"		"Freeze Camera"
			"help"		"Enable freeze cam features? Available only for Prop that attacked by Hunter."
			"type"		"CheckBox"
			"default"	"1"
		}
		
		10
		{
			"name"		"ph_better_prop_movement"
			"text"		"Better Prop Movement"
			"help"		"Should Enable Prop Rotation or Not? (Originally facing to 0 0 0). Props rotate and are seen properly by the local player."
			"type"		"CheckBox"
			"default"	"1"
		}
		
		11
		{
			"name"		"ph_prop_collision"
			"text"		"Team Props Collision"
			"help"		"Should Team Props collide with each other?"
			"type"		"CheckBox"
			"default"	"1"
		}
		
		12
		{
			"name"		"ph_prop_additional_models"
			"text"		"Team Props Additional Models"
			"help"		"Should Team Props have different starting models instead of Kleiner?"
			"type"		"CheckBox"
			"default"	"0"
		}
		13
		{
			"name"		"ph_use_custom_plmodel"
			"text"		"Hunters Custom Models"
			"help"		"Should Team Hunters have Custom models?"
			"type"		"CheckBox"
			"default"	"0"
		}
		14
		{
			"name"		"ph_use_custom_plmodel_for_prop"
			"text"		"Props Custom Models"
			"help"		"Should Team Props have Custom models? (Enable for Hunter first!)"
			"type"		"CheckBox"
			"default"	"0"
		}

	}

}
